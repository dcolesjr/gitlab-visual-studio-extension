﻿using NUnit.Framework;
using System;
using GitLab.Extension.SettingsUtil;
using Microsoft.Win32;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;

namespace GitLab.Extension.Tests
{
    [TestFixture]
    internal class SettingsTests
    {
        private bool _onSettingsChangedEventCalled = false;
        private Settings.SettingsEventArgs _onSettingsChangedEventArgs;

        [SetUp]
        public void Setup()
        {
            _onSettingsChangedEventCalled = false;
            _onSettingsChangedEventArgs = null;
        }

        [TearDown]
        public void Teardown()
        {
            TestData.ResetSettings();
        }

        #region Helper methods

        public class TestNullProtect : ISettingsProtect
        {
            public string Protect(string data)
            {
                return data;
            }

            public string Unprotect(string protectedData)
            {
                return protectedData;
            }
        }

        private void OnSettingsChangedEvent(object sender, EventArgs e)
        {
            _onSettingsChangedEventArgs = e as Settings.SettingsEventArgs;
            _onSettingsChangedEventCalled = true;
        }

        private static bool DoesRegistryKeyExist(string registryKey)
        {
            var key = Registry.CurrentUser.OpenSubKey($"Software\\{registryKey}");
            return key != null;
        }

        private static string Protect(string data)
        {
            try
            {
                var dataAsBytes = UTF8Encoding.UTF8.GetBytes(data);
                var protectedAsBytes = ProtectedData.Protect(dataAsBytes, null, DataProtectionScope.CurrentUser);
                return System.Convert.ToBase64String(protectedAsBytes);
            }
            catch (CryptographicException e)
            {
                Trace.WriteLine($"Settings: Protect exception {e}");
                return string.Empty;
            }
        }
        
        #endregion


        [Test]
        public void ConfiguredTest()
        {
            Settings.Instance.GitLabAccessToken = string.Empty;
            Assert.IsFalse(Settings.Instance.Configured, "Expected Configured to return false when access token isn't set");
            Settings.Instance.GitLabAccessToken = "glpat-abcdef";
            Assert.IsTrue(Settings.Instance.Configured, "Expected Configured to return true when access token is set");
        }

        [Test]
        public void SettingsChangedEventTest()
        {
            Settings.Instance.SettingsChangedEvent += OnSettingsChangedEvent;
            try
            {
                Settings.Instance.GitLabAccessToken = "glpat-abcdef";
                Assert.IsTrue(_onSettingsChangedEventCalled,
                    "Expected OnSettingsChangedEvent handler to be called when settings change.");
                Assert.AreEqual(Settings.GitLabAccessTokenKey, _onSettingsChangedEventArgs.ChangedSettingKey,
                    $"Expected ChangedSettingKey to be '{Settings.GitLabAccessTokenKey}' instead got '{_onSettingsChangedEventArgs.ChangedSettingKey}'");

                _onSettingsChangedEventCalled = false;

                Settings.Instance.IsCodeSuggestionsEnabled = false;
                Assert.IsTrue(_onSettingsChangedEventCalled,
                    "Expected OnSettingsChangedEvent handler to be called when settings change.");
                Assert.AreEqual(Settings.IsCodeSuggestionsEnabledKey, _onSettingsChangedEventArgs.ChangedSettingKey,
                    $"Expected ChangedSettingKey to be '{Settings.IsCodeSuggestionsEnabledKey}' instead got '{_onSettingsChangedEventArgs.ChangedSettingKey}'");

                _onSettingsChangedEventCalled = false;

                Settings.Instance.GitLabUrl = "https://abc.com";
                Assert.IsTrue(_onSettingsChangedEventCalled,
                    "Expected OnSettingsChangedEvent handler to be called when settings change.");
                Assert.AreEqual(Settings.GitLabUrlKey, _onSettingsChangedEventArgs.ChangedSettingKey,
                    $"Expected ChangedSettingKey to be '{Settings.GitLabUrlKey}' instead got '{_onSettingsChangedEventArgs.ChangedSettingKey}'");
            }
            finally
            {
                Settings.Instance.SettingsChangedEvent -= OnSettingsChangedEvent;
            }
        }

        [Test]
        public void ConvertPoCSettingsTest()
        {
            if(DoesRegistryKeyExist(Settings.ApplicationName))
                Registry.CurrentUser.DeleteSubKey($"Software\\{Settings.ApplicationName}");

            var key = Registry.CurrentUser.CreateSubKey($"Software\\{Settings.ApplicationNamePoC}", true);
            key.SetValue(Settings.GitLabAccessTokenKey,
                Protect(TestData.CodeSuggestionsToken), RegistryValueKind.String);

            Assert.IsTrue(DoesRegistryKeyExist(Settings.ApplicationNamePoC),
                "Expect PoC registry key to exist");
            Assert.IsFalse(DoesRegistryKeyExist(Settings.ApplicationName),
                "Expect settings registry key to not exist");

            var settings = Settings.Instance as Settings;
            settings.Storage.Load(settings);

            Assert.IsFalse(DoesRegistryKeyExist(Settings.ApplicationNamePoC),
                "Expecte PoC registry key to have been deleted");
            Assert.IsTrue(DoesRegistryKeyExist(Settings.ApplicationName),
                "Expect settings registry key to exist");
        }
    }
}
