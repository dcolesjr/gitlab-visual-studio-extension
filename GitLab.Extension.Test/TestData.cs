﻿using NUnit.Framework;
using System;
using System.IO;
using GitLab.Extension.SettingsUtil;
using static GitLab.Extension.Tests.SettingsTests;

namespace GitLab.Extension.Tests
{
    /// <summary>
    /// Data or settings needed to run tests.
    /// </summary>
    [SetUpFixture]
    internal class TestData
    {
        /// <summary>
        /// Token that can be provided to language server for code suggestions access.
        /// This is usually a personal access token with read_api and read_user privs.
        /// Make sure you have enabled code suggestions in your account settings.
        /// </summary>
        public static string CodeSuggestionsToken {get; private set;}

        /// <summary>
        /// Our solution name 'GitLab.Extension'
        /// </summary>
        public static string SolutionName { get; } = "GitLab.Extension";

        /// <summary>
        /// Path to our solutions folder.
        /// </summary>
        public static string SolutionPath { get; private set; }

        public static string GitLabUrl { get; private set; }

        /// <summary>
        /// Store configured access token
        /// </summary>
        private string _userAccessToken = string.Empty;

        /// <summary>
        /// Store configured code suggestions enabled
        /// </summary>
        private bool _codeSuggestionsEnabled = true;

        /// <summary>
        /// Store configured GitLab URL
        /// </summary>
        private string _gitlabUrl = string.Empty;

        /// <summary>
        /// The process name of a language server instance.
        /// This is the executable name w/o the .exe.
        /// </summary>
     
        public static string LanguageServerProcessName { get; } = "gitlab-code-suggestions-language-server-windows-amd64";

        static TestData()
        {
            GitLabUrl = Environment.GetEnvironmentVariable("GITLAB_SERVER");
            if (GitLabUrl == null)
                GitLabUrl = "https://gitlab.com";

            CodeSuggestionsToken = Environment.GetEnvironmentVariable("GITLAB_TOKEN");
            if (CodeSuggestionsToken == null)
                throw new ArgumentException("Required environment variable 'GITLAB_TOKEN' was not found. This is required to run the tests.");

            SolutionPath = FindSolutionDirectory();
        }

        static string FindSolutionDirectory()
        {
            var dir = Environment.CurrentDirectory;

            while (!File.Exists(Path.Combine(dir, $"{SolutionName}.sln")))
            {
                dir = Path.GetDirectoryName(dir);
                if(dir == null)
                    throw new ApplicationException($"Expected tests to be run from a directory that is a child of the solution folder. Unable to locate '{SolutionName}.sln' in parent directories.");
            }

            return dir;
        }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            // If we are in a CI Pipeline the Protect API will nor work
            // so use a null version instead.
            if (InCiPipeline())
            {
                var settings = Settings.Instance as Settings;
                settings.Storage = new RegistryStorage(new TestNullProtect());
            }

            if (Settings.Instance.Configured)
            {
                _gitlabUrl = Settings.Instance.GitLabUrl;
                _userAccessToken = Settings.Instance.GitLabAccessToken;
                _codeSuggestionsEnabled = Settings.Instance.IsCodeSuggestionsEnabled;
            }

            Settings.Instance.GitLabUrl = GitLabUrl;
            Settings.Instance.GitLabAccessToken = CodeSuggestionsToken;
        }

        [OneTimeTearDown]
        public void OneTimeTeardown()
        {
            Settings.Instance.GitLabUrl = _gitlabUrl;
            Settings.Instance.GitLabAccessToken = _userAccessToken;
            Settings.Instance.IsCodeSuggestionsEnabled = _codeSuggestionsEnabled;
        }

        public static void ResetSettings()
        {
            Settings.Instance.GitLabUrl = GitLabUrl;
            Settings.Instance.GitLabAccessToken = CodeSuggestionsToken;
            Settings.Instance.IsCodeSuggestionsEnabled = true;
        }

        private bool InCiPipeline()
        {
            return !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("CI_COMMIT_SHA"));
        }
    }
}
