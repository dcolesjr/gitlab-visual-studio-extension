﻿using GitLab.Extension.LanguageServer;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLab.Extension.Tests.LanguageServer
{
    [TestFixture]
    internal class ExponentialBackoffHelperTests
    {
        /// <summary>
        /// Allow incrementing retries without actually delaying execution.
        /// </summary>
        internal class BackoffTestHelper : ExponentialBackoffHelper
        {
            public void Next()
            {
                _retries++;
            }
        }

        [Test]
        public void FirstDelayIsZero()
        {
            var backoff = new ExponentialBackoffHelper();
            Assert.AreEqual(0, backoff.DelayMilliseconds, $"Expected first delay to be zero, but got {backoff.DelayMilliseconds}");
        }

        [Test]
        public void ExponetialDelayIncreaseTest()
        {
            var expected = new int[]
            {
                0,
                300,
                600,
                1200,
                2400,
                4800,
                9600,
                19200,
                38400,
                76800,
                153600,
                307200,
                614400,
                1228800,
                2457600            
            };

            var backoff = new BackoffTestHelper();
            
            foreach(var expectedDelay in expected)
            {
                Assert.AreEqual(expectedDelay, backoff.DelayMilliseconds,
                    $"Expected a delay of {expectedDelay} but got a delay of {backoff.DelayMilliseconds}.");
                backoff.Next();
            }
        }

        [Test]
        public async Task ReturnFalseOnMaxRetriesAsync()
        {
            var backoff = new BackoffTestHelper();

            for(var cnt = 0; cnt < 15; cnt++)
                backoff.Next();

            Assert.IsFalse(await backoff.DelayAsync(),
                "Expected a return of false when max retries is hit.");
        }
    }
}
