﻿using GitLab.Extension.LanguageServer;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace GitLab.Extension.Tests.LanguageServer
{
    [TestFixture]
    internal class LsProcessManagerTests
    {
        [Test]
        public async Task StartAndStopAsync()
        {
            Assert.IsFalse(
                LsCommon.IsLanguageServerRunning(out _), 
                "Expected that no langauge servers would exist before test is run");

            var ret = LsProcessManager.Instance.StartLanguageServer(
                TestData.SolutionPath, TestData.GitLabUrl, TestData.CodeSuggestionsToken, 
                out var firstPort);

            Assert.IsTrue(ret, "Expected StartLanguageServer to return true.");
            Assert.IsTrue(
                LsCommon.IsLanguageServerRunning(out var count), 
                "Expected language server to have been started.");
            Assert.AreEqual(1, count, $"Expected 1 process, but got {count} processes.");

            try
            {
                ret = LsProcessManager.Instance.StartLanguageServer(
                    TestData.SolutionPath, TestData.GitLabUrl, TestData.CodeSuggestionsToken, 
                    out var secondPort);

                Assert.IsFalse(ret, "Expected second StartLangaugeServer call to return false.");
                Assert.AreEqual(firstPort, secondPort, $"Expected firstPort ({firstPort}) and secondPort ({secondPort}) to be the same.");
                LsCommon.IsLanguageServerRunning(out count);
                Assert.AreEqual(1, count, $"Expected 1 process after second StartLangauageServer, but got {count} processes.");
            }
            finally
            {
                ret = await LsProcessManager.Instance.StopLanguageServerAsync(TestData.SolutionPath);

                Assert.IsTrue(ret, "Expected StopLanguageServerAsync to return true.");
                Assert.IsFalse(
                    LsCommon.IsLanguageServerRunning(TimeSpan.FromSeconds(10), out _),
                    "Expected the language server process to have been stopped.");
            }

            // Make sure starting it again works as expected

            // Hold the firstPort so we can verify we get a different port
            LsCommon.HoldPort(firstPort);
            try
            {
                ret = LsProcessManager.Instance.StartLanguageServer(
                    TestData.SolutionPath, TestData.GitLabUrl, TestData.CodeSuggestionsToken, 
                    out var thirdPort);

                Assert.IsTrue(ret, "Expected StartLanguageServer to return true.");
                Assert.IsTrue(
                    LsCommon.IsLanguageServerRunning(out count),
                    "Expected language server to have been started.");
                Assert.AreEqual(1, count, $"Expected 1 process, but got {count} processes.");
                Assert.AreNotEqual(firstPort, thirdPort,
                    $"Expected firstPort ({firstPort}) and thirdPort ({thirdPort}) to be different. firstPort was held and could not be used by a new LS.");

                try
                {
                    ret = LsProcessManager.Instance.StartLanguageServer(
                        TestData.SolutionPath, TestData.GitLabUrl, TestData.CodeSuggestionsToken, 
                        out var fourthPort);

                    Assert.IsFalse(ret, "Expected second StartLangaugeServer call to return false.");
                    Assert.AreEqual(thirdPort, fourthPort, $"Expected thirdPort ({thirdPort}) and fourthPort ({fourthPort}) to be the same.");
                    LsCommon.IsLanguageServerRunning(out count);
                    Assert.AreEqual(1, count, $"Expected 1 process after second StartLangauageServer, but got {count} processes.");
                }
                finally
                {
                    ret = await LsProcessManager.Instance.StopLanguageServerAsync(TestData.SolutionPath);
                    Assert.IsTrue(ret, "Expected StopLanguageServerAsync to return true.");
                    Assert.IsFalse(
                        LsCommon.IsLanguageServerRunning(TimeSpan.FromSeconds(10), out _),
                        "Expected the language server process to have been stopped.");
                }
            }
            finally
            {
                LsCommon.ReleasePort(firstPort);
            }
        }
    }
}
