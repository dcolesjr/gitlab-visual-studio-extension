﻿using GitLab.Extension.LanguageServer;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLab.Extension.Tests.LanguageServer
{
    [TestFixture]
    internal class LsClientManagerTests
    {
        [TearDown]
        public async Task TeardownAsync()
        {
            await LsClientManager.Instance.DisposeClientAsync(TestData.SolutionName);
        }

        [Test]
        public void SingleLsClientInstancePerSolutionTest()
        {
            var lsClient1 = LsClientManager.Instance.GetClient(TestData.SolutionName, TestData.SolutionPath);
            var lsClient2 = LsClientManager.Instance.GetClient(TestData.SolutionName, TestData.SolutionPath);

            Assert.AreEqual(lsClient1, lsClient2, "Expected the same LsClient instance");
        }

        [Test]
        public async Task DifferentLsClientInstanceAfterDisposeTestAsync()
        {
            var lsClient1 = LsClientManager.Instance.GetClient(TestData.SolutionName, TestData.SolutionPath);
            await LsClientManager.Instance.DisposeClientAsync(TestData.SolutionName);
            var lsClient2 = LsClientManager.Instance.GetClient(TestData.SolutionName, TestData.SolutionPath);

            Assert.AreNotEqual(lsClient1, lsClient2, "Expected a different LsClient instance");
        }
    }
}
