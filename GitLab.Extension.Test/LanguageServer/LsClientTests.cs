﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.SettingsUtil;
using System.Threading;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace GitLab.Extension.Tests.LanguageServer
{
    [TestFixture]
    internal class LsClientTests
    {
        [SetUp]
        public void Setup()
        {
            Assert.IsFalse(LsCommon.IsLanguageServerRunning(out var _), 
                "Expected no language servers to be running at start of test (Setup)");
        }

        [TearDown]
        public void Teardown()
        {
            Assert.IsFalse(LsCommon.IsLanguageServerRunning(TimeSpan.FromSeconds(10), out var _), 
                "Expected no language servers to be running at end of test (Teardown)");

            TestData.ResetSettings();
        }

        public async Task<LsClient> StartLsClientAsync()
        {
            var lsClient = new LsClient(TestData.SolutionName, TestData.SolutionPath);
            var ret = await lsClient.ConnectAsync();
            Assert.IsTrue(ret, "Expected LsClient to connect");

            return lsClient;
        }

        public static string GetTempFilePathWithExtension(string extension)
        {
            var path = Path.GetTempPath();
            string fileName;
            string tmpFile;

            do
            {
                fileName = Path.ChangeExtension(Guid.NewGuid().ToString(), extension);
                tmpFile = Path.Combine(path, fileName);
            }
            while (File.Exists(tmpFile));

            return tmpFile;
        }

        public static string GetFileUrl(string fileName)
        {
            return $"file://{fileName.Replace('\\', '/')}";
        }

        [Test]
        public async Task ConnectTestAsync()
        {
            using (var lsClient = new LsClient(TestData.SolutionName, TestData.SolutionPath))
            {
                var ret = await lsClient.ConnectAsync();
                Assert.IsTrue(ret, "Expected LsClient to connect");
            }
        }

        [Test]
        public async Task DisposeWithOutConnectTestAsync()
        {
            var lsClient = new LsClient(TestData.SolutionName, TestData.SolutionPath);
            await lsClient.DisposeAsync();
        }

        [Test]
        public async Task ReconnectionTestAsync()
        {
            using (var lsClient = await StartLsClientAsync())
            {
                await LsCommon.VerifyLsClientWorkingAsync(lsClient);
                
                LsCommon.KillLsClientAndWaitForReconnect(lsClient);

                await LsCommon.VerifyLsClientWorkingAsync(lsClient);
            }
        }

        [Test]
        public async Task TextDocumentDidOpenAsync()
        {
            using (var lsClient = await StartLsClientAsync())
            {
                Assert.IsTrue(await lsClient.SendTextDocumentDidOpenAsync(
                        "file:///foo.cs",
                        0,
                        "namespace Bar { internal class Foo { } }"),
                    "Expected SendTextDocumentDidOpenAsync to return true");
            }
        }

        [Test]
        public async Task TextDocumentDidChangeAsync()
        {
            using (var lsClient = await StartLsClientAsync())
            {
                Assert.IsTrue(await lsClient.SendTextDocumentDidOpenAsync(
                        "file:///foo.cs",
                        0,
                        "namespace Bar { internal class Foo { } }"),
                    "Expected SendTextDocumentDidOpenAsync to return true");

                Assert.IsTrue(await lsClient.SendTextDocumentDidChangeAsync(
                        "file:///foo.cs",
                        1,
                        "namespace Bar { internal class Foo { public Foo() { } } }"
                        ),
                    "Expected SendTextDocumentDidChangeAsync to return true");
            }
        }

        [Test]
        public async Task TextDocumentCompletionAsync()
        {
            var tmpFile = GetTempFilePathWithExtension(".cs");
            var codeV0 = "namespace Bar {\n\tinternal class Foo {\n\t}\n}";
            var codeV1 = "namespace Bar {\n\tinternal class Foo {\n\t\tpublic Foo() {\n\t\t\t\n\t\t}\n\t}\n}";
            uint codeV1Line = 3;
            uint codeV1Pos = 3;
            try
            {
                File.WriteAllText(tmpFile, "namespace Bar {\n\tinternal class Foo {\n\t\tpublic Foo() {\n\t\t}\n\t}\n}");

                using (var lsClient = await StartLsClientAsync())
                {
                    bool ret;
                    var tokenSource = new CancellationTokenSource();

                    ret = await lsClient.SendTextDocumentDidOpenAsync(
                        tmpFile,
                        0,
                        codeV0);
                    Assert.IsTrue(ret, "Expect SendTextDocumentDidOpenAsync to return true");

                    ret = await lsClient.SendTextDocumentDidChangeAsync(
                        tmpFile,
                        1,
                        codeV1);
                    Assert.IsTrue(ret, "Expect SendTextDocumentDidChangeAsync to return true");

                    // Make sure the didChange gets processed before asking for completion
                    await Task.Delay(250);

                    var (completions, error) = await lsClient.SendTextDocumentCompletionAsync(
                        tmpFile,
                        codeV1Line,
                        codeV1Pos,
                        tokenSource.Token);

                    Assert.IsNotNull(completions, "Expected a non-null result from SendTextDocumentCompletionAsync");
                    Assert.GreaterOrEqual(completions.Length, 1, "Expected at least one completion.");
                    Assert.NotZero(completions[0].insertText.Length, "Expected completion inserText to be longer than zero.");
                }
            }
            finally
            {
                if(File.Exists(tmpFile))
                    File.Delete(tmpFile);
            }
        }

        [Test]
        public async Task RestartLsProcessWhenSettingsChangeAsync()
        {
            var initialAccessToken = Settings.Instance.GitLabAccessToken;

            try
            {
                using (var lsClient = await StartLsClientAsync())
                {
                    var initialProcessId = LsCommon.GetLangaugeServerProcessId();

                    Settings.Instance.GitLabAccessToken = "glpat-abcdef";

                    // Allow upto 5 seconds for the LS to be restarted

                    var afterProcessId = -1;
                    var startTime = DateTime.Now;
                    var waitTime = TimeSpan.FromSeconds(5);
                    do
                    {
                        afterProcessId = LsCommon.GetLangaugeServerProcessId();
                    }
                    while((initialProcessId == afterProcessId || afterProcessId == -1) &&
                        (DateTime.Now - startTime < waitTime));

                    Assert.AreNotEqual(-1, afterProcessId, "Expected a langauge server process to be found after settings change");
                    Assert.AreNotEqual(initialProcessId, afterProcessId, "Expected the LS to have been restarted and have a different process ID");
                }
            }
            finally
            {
                Settings.Instance.GitLabAccessToken = initialAccessToken;
            }
        }

        private string _userAgent;

        private void UserAgentTestHandler(HttpListenerRequest request, HttpListenerResponse response)
        {
            _userAgent = request.Headers["user-agent"];

            response.StatusCode = 200;
            response.StatusDescription = "OK";
            response.ContentLength64 = 0;
        }

        [Test]
        public async Task UserAgentTestAsync()
        {
            _userAgent = null;

            using (var listener = new TestHttpListener(UserAgentTestHandler))
            {
                var tmpFile = GetTempFilePathWithExtension(".cs");
                var codeV0 = "namespace Bar {\n\tinternal class Foo {\n\t}\n}";
                var codeV1 = "namespace Bar {\n\tinternal class Foo {\n\t\tpublic Foo() {\n\t\t\t\n\t\t}\n\t}\n}";
                uint codeV1Line = 3;
                uint codeV1Pos = 3;

                try
                {
                    File.WriteAllText(tmpFile, "namespace Bar {\n\tinternal class Foo {\n\t\tpublic Foo() {\n\t\t}\n\t}\n}");

                    Settings.Instance.GitLabUrl = listener.Url;

                    using (var lsClient = await StartLsClientAsync())
                    {
                        bool ret;
                        var tokenSource = new CancellationTokenSource();

                        ret = await lsClient.SendTextDocumentDidOpenAsync(
                            tmpFile,
                            0,
                            codeV0);
                        Assert.IsTrue(ret, "Expect SendTextDocumentDidOpenAsync to return true");

                        ret = await lsClient.SendTextDocumentDidChangeAsync(
                            tmpFile,
                            1,
                            codeV1);
                        Assert.IsTrue(ret, "Expect SendTextDocumentDidChangeAsync to return true");

                        // Make sure the didChange gets processed before asking for completion
                        await Task.Delay(250);

                        var (completions, error) = await lsClient.SendTextDocumentCompletionAsync(
                            tmpFile,
                            codeV1Line,
                            codeV1Pos,
                            tokenSource.Token);
                    }
                }
                catch(Exception ex)
                {
                    if (ex.Message.Contains("unexpected 200 OK response when acquiring token"))
                        return;

                    throw;
                }
                finally
                {
                    if (File.Exists(tmpFile))
                        File.Delete(tmpFile);
                }

                Assert.IsNotNull(_userAgent, "Expected a non-null user-agent");
                Assert.IsTrue(_userAgent.Contains("(gl-visual-studio-extension:"),
                    $"Expected the user-agent to contain '(gl-visual-studio-extension:' but instead got '{_userAgent}'");
            }
        }
    }
}
