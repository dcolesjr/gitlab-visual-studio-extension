﻿using GitLab.Extension.LanguageServer;
using Microsoft.VisualStudio.VCProjectEngine;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Linq;

namespace GitLab.Extension.Tests.LanguageServer
{
    [SetUpFixture]
    internal class LsCommon
    {
        private static int[] _existingProcessIds = null;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var processes = Process.GetProcessesByName(TestData.LanguageServerProcessName);
            if (processes.Length == 0)
                _existingProcessIds = new int[0]; 

            var processIds = new List<int>(processes.Length);
            foreach(var p in processes)
            {
                processIds.Add(p.Id);
            }

            _existingProcessIds = processIds.ToArray();
        }

        public static Process[] FilterOutExistingProcesses(Process[] processes)
        {
            if (processes.Length == 0)
                return new Process[0];

            var ret = new List<Process>();
            foreach(var p in processes)
            {
                if (((IList<int>)_existingProcessIds).Contains(p.Id))
                    continue;

                ret.Add(p);
            }

            return ret.ToArray();
        }

        /// <summary>
        /// Get language server processes excluding those that existed
        /// before running out tests.
        /// </summary>
        /// <returns></returns>
        public static Process[] GetLanguageServerProcesses()
        {
            return FilterOutExistingProcesses(
                Process.GetProcessesByName(TestData.LanguageServerProcessName));
        }

        /// <summary>
        /// Does a language server process exist?
        /// </summary>
        /// <returns>True if process exists, false otherwise.</returns>
        public static bool IsLanguageServerRunning(out int count)
        {
            var processes = GetLanguageServerProcesses();
            count = processes.Length;
            return processes.Length > 0;
        }

        /// <summary>
        /// Get the language server process id
        /// </summary>
        /// <returns>Process id or -1 if process not found or multiple processes found.</returns>
        public static int GetLangaugeServerProcessId()
        {
            var processes = GetLanguageServerProcesses();
            if (processes == null || processes.Length != 1)
                return -1;

            return processes[0].Id;
        }

        /// <summary>
        /// Does a language server process exist?
        /// </summary>
        /// <param name="waitTime">Time to wait for process to stop if its found.</param>
        /// <returns>True if process exists, false otherwise.</returns>
        public static bool IsLanguageServerRunning(TimeSpan waitTime, out int count)
        {
            var start = DateTime.Now;

            do
            {
                if (!IsLanguageServerRunning(out count))
                    return false;
            }
            while (DateTime.Now - start < waitTime);

            return true;
        }

        static Dictionary<int, TcpListener> _holdPorts = new Dictionary<int, TcpListener>();

        public static void HoldPort(int port)
        {
            var _listener = new TcpListener(IPAddress.Loopback, port);
            _listener.Start();

            _holdPorts[port] = _listener;
        }

        public static void ReleasePort(int port)
        {
            var _listener = _holdPorts[port];
            _holdPorts.Remove(port);

            _listener.Stop();
            _listener.Server?.Dispose();
        }

        public static void KillLanguageServer()
        {
            var processes = GetLanguageServerProcesses();
            foreach (var process in processes)
            {
                process.Kill();
                process.WaitForExit();
            }
        }

        public static void KillLsClientAndWaitForReconnect(LsClient lsClient)
        {
            KillLanguageServer();
            var maxWait = TimeSpan.FromSeconds(5);
            var ret = false;

            for (var start = DateTime.Now; DateTime.Now - start < maxWait;)
            {
                ret = IsLanguageServerRunning(out var _);
                if (!ret)
                    continue;

                if (lsClient.IsConnected)
                    break;
            }

            Assert.IsTrue(ret, "Expected language server client to be connected.");
        }
        public static async Task VerifyLsClientWorkingAsync(LsClient lsClient)
        {
            Assert.IsTrue(await lsClient.SendTextDocumentDidOpenAsync(
                "file:///foo.cs",
                0,
                "namespace Bar { internal class Foo { } }"),
                "Expected SendTextDocumentDidOpenAsync call to work");

            Assert.IsTrue(await lsClient.SendTextDocumentDidChangeAsync(
                    "file:///foo.cs",
                    1,
                    "namespace Bar { internal class Foo { public Foo() { } } }"
                ),
                "Expected first SendTextDocumentDidChangeAsync to work");
        }

    }
}
