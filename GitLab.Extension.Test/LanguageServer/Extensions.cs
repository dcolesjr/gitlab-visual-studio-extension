﻿using GitLab.Extension.LanguageServer;
using Microsoft.VisualStudio.PlatformUI;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLab.Extension.Tests.LanguageServer
{
    [TestFixture]
    internal class Extensions
    {
        [Test]
        public void SplitToLinesTest()
        {
            var input = "line 1\nline 2\nline 3\n";
            var expected = new string[]
            {
                "line 1",
                "line 2",
                "line 3",
            };

            var actual = input.SplitToLines();

            Assert.AreEqual(expected, actual, $"Did not get expected output, actual is: {actual}");
        }
    }
}
