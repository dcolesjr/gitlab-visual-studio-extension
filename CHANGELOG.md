## 0.2.0 (2023-06-22)

No changes.

## 0.1.0 (2023-06-22)

### added (1 change)

- [Add output/buildtag file](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e3ad6661962023498db90b7340f24c52e313476e) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!16))

## 0.0.1 (2023-06-22)

### added (1 change)

- [Add output/buildtag file](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e3ad6661962023498db90b7340f24c52e313476e) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!16))
