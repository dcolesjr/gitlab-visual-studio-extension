# Build with Debug target for tests, and with Release for the VSIX installer.
& $env:MSBUILD_PATH GitLab.Extension.sln /target:Clean /target:Build /p:DeployExtension=false /p:Configuration=Debug
& $env:MSBUILD_PATH GitLab.Extension.sln /target:Clean /target:Build /p:DeployExtension=false /p:Configuration=Release
