﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("GitLab Visual Studio Extension")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GitLab, LLC")]
[assembly: AssemblyProduct("GitLab Visual Studio Extension")]
[assembly: AssemblyCopyright("Copyright (c) GitLab, LLC")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: InternalsVisibleTo("GitLab.Extension.Tests,PublicKey=00240000048000009400000006020000002400005253413100040000010001000db559f0a8f2ee85a6734f5be17a02a3ced87c7f8089bd18a98b72a1ddc3813e7e9085dfea1946ebd6376158550a51fcaddf64a3331865f0a0ed2bfb4d6016408316c3bd2589544e8d51a7026f6db2a5d4a03b64a55c6977f1fb5cdf3322c2af4903b253144a65789cda8650631cc52cd6f9d82f6e39596bd968da2232f318b9", AllInternalsVisible =true)]

