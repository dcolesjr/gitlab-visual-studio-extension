﻿using EnvDTE80;
using GitLab.Extension.LanguageServer;
using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;
using Microsoft.VisualStudio.Threading;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using GitLab.Extension.SettingsUtil;

namespace GitLab.Extension.CodeSuggestions
{
    /// <summary>
    /// Generate inline proposals for a unique IWpfTextView instance.
    /// Each open file will have it's own GitlabProposalSource instance.
    /// When the file is closed, the associated GitlabProposalSource instance will be disposed.
    /// </summary>
    internal class GitlabProposalSource : ProposalSourceBase
    {
        private const string TracePrefix = "ProposalSource:";

        private readonly string _sourceName = nameof(GitlabProposalSource);
        private readonly IWpfTextView _textView;

        private bool _disposed = false;
        private string _fullFilePath;
        private LsClient _lsClient;
        private string _relativeFilePath;
        private string _solutionName;
        private string _solutionPath;

        public GitlabProposalSource(IWpfTextView textView)
        {
            _textView = textView;
            var uiElement = _textView as UIElement;

#pragma warning disable VSTHRD001 // Avoid legacy thread switching APIs
            uiElement.Dispatcher.Invoke(new Action(() =>
            {
                ThreadHelper.ThrowIfNotOnUIThread();

                GetSolutionNameAndPath(out _solutionName, out _solutionPath);

                _fullFilePath = GetTextViewFilePath();
                if (_fullFilePath.StartsWith(_solutionPath))
                    _relativeFilePath = _fullFilePath.Substring(_solutionPath.Length + 1);
                else
                    _relativeFilePath = _fullFilePath;
            }));
#pragma warning restore VSTHRD001 // Avoid legacy thread switching APIs

            _textView.TextBuffer.Changed += TextBuffer_ChangedAsync;
        }

        public async Task<bool> StartLanguageServerClientAsync()
        {
            if (!Settings.Instance.Configured)
            {
                Status.StatusBar.Instance.CodeSuggestionsError("Extension not configured.", true);
                return false;
            }

            if (!Settings.Instance.IsCodeSuggestionsEnabled)
                return false;

            _lsClient = LsClientManager.Instance.GetClient(_solutionName, _solutionPath);
            await _lsClient.ConnectAsync();
            await TextBuffer_OpenedAsync();

            Status.StatusBar.Instance.CodeSuggestionsClearError();

            return true;
        }

        /// <summary>
        /// Get the filename we are editing
        /// </summary>
        /// <returns></returns>
        private string GetTextViewFilePath()
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            _textView.TextBuffer.Properties.TryGetProperty(typeof(IVsTextBuffer), out IVsTextBuffer bufferAdapter);
            var persistFileFormat = bufferAdapter as IPersistFileFormat;

            if (persistFileFormat == null)
                return null;

            persistFileFormat.GetCurFile(out var filepath, out _);

            if (string.IsNullOrEmpty(filepath))
                return null;

            return filepath;
        }

        private async Task TextBuffer_OpenedAsync()
        {
            if (!Settings.Instance.IsCodeSuggestionsEnabled)
                return;

            if (_lsClient == null && !await StartLanguageServerClientAsync())
                return;

            await _lsClient.SendTextDocumentDidOpenAsync(
                _relativeFilePath,
                _textView.TextBuffer.CurrentSnapshot.Version.VersionNumber,
                _textView.TextBuffer.CurrentSnapshot.GetText());
        }

#pragma warning disable VSTHRD100
#pragma warning disable VSTHRD200
        /// <summary>
        /// Send changes to language server.
        /// </summary>
        /// <remarks>
        /// This async method must return void to be a valid event handler.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void TextBuffer_ChangedAsync(object sender, TextContentChangedEventArgs e)
        {
            try
            {
                if (!Settings.Instance.IsCodeSuggestionsEnabled)
                    return;

                if (_lsClient == null && !await StartLanguageServerClientAsync())
                    return;

                //var changedText = e.After.GetText();
                var versionNumber = e.AfterVersion.VersionNumber;
                var fullText = e.AfterVersion.TextBuffer.CurrentSnapshot.GetText();

                await _lsClient.SendTextDocumentDidChangeAsync(
                    _relativeFilePath,
                    versionNumber,
                    fullText);
            }
            catch(Exception ex)
            {
                // Don't crash visual studio by handling all exceptions
                Trace.WriteLine($"{TracePrefix} TextBuffer_ChangedAsync Exception: {ex}");
            }
        }
#pragma warning restore VSTHRD100
#pragma warning restore VSTHRD200

        /// <summary>
        /// Called by Visual Studio to get a list of proposals from our extension. This
        /// triggers a call to get a code suggestion.
        /// </summary>
        /// <param name="caret"></param>
        /// <param name="completionState"></param>
        /// <param name="scenario"></param>
        /// <param name="triggeringCharacter"></param>
        /// <param name="token"></param>
        /// <returns>Returns a GitlabProposalCollection instance or null on error.</returns>
        public override async Task<ProposalCollectionBase> RequestProposalsAsync(
            VirtualSnapshotPoint caret, 
            CompletionState completionState, 
            ProposalScenario scenario, 
            char triggeringCharacter, 
            CancellationToken token)
        {
            // Return null to avoid the possiblity of Visual Studio 
            // seeing an exception as a bug and reporting it to the user.
            if (_disposed)
                return null;

            // We can't provide code suggestions until the extension is configured
            if (!Settings.Instance.Configured)
                return null;

            // Don't request a suggestion if code suggestions are disabled
            if (!Settings.Instance.IsCodeSuggestionsEnabled)
                return null;

            var error = null as string;

            try
            {
                Status.StatusBar.Instance.CodeSuggestionsInProgressStart();

                // If the extension was just configured, we will need
                // to start the language server.
                if (_lsClient == null && !await StartLanguageServerClientAsync())
                        return null;

                // Wait a querter of a second in case the user is typing quickly.
                token.WaitHandle.WaitOne(150);
                if (token.IsCancellationRequested)
                    return null;

                // Get code suggestion from language service
                var ret = await GetCodeSuggestionAsync(token);
                error = ret.error;
                if (ret.completion == null || token.IsCancellationRequested)
                    return null;

                return CreateProposalCollection(caret, completionState, ret.completion);
            }
            finally
            {
                Status.StatusBar.Instance.CodeSuggestionsInProgressComplete();
                if (error != null)
                    Status.StatusBar.Instance.CodeSuggestionsError(error);
            }
        }

        /// <summary>
        /// Called when a code suggestion has been accepted to commit the change to the buffer.
        /// </summary>
        /// <param name="edits"></param>
        private void CommitSuggestion(string suggestion)
        {
            // todo -- add metric for accepted suggestions
            _textView.TextBuffer.Insert(_textView.Caret.Position.BufferPosition, suggestion);
        }

        /// <summary>
        /// Create a GitlabProposalCollection from a code suggestion
        /// </summary>
        /// <param name="caret"></param>
        /// <param name="completionState"></param>
        /// <param name="suggestion"></param>
        /// <returns></returns>
        private ProposalCollection CreateProposalCollection(
            VirtualSnapshotPoint caret,
            CompletionState completionState, 
            string suggestion)
        {
            // Create proposals
            var proposalCaret = _textView.Caret.Position.VirtualBufferPosition;
            var flags = ProposalFlags.FormatAfterCommit | ProposalFlags.MoveCaretToEnd | ProposalFlags.DisableInIntelliSense;
            var displaySuggestion = suggestion;

            var bufferPosition = _textView.Caret.Position.BufferPosition;
            var line = bufferPosition.GetContainingLine();
            var column = bufferPosition.Position - line.Start.Position;

            // If the caret is in virtual space and the column position is zero,
            // it will look to the user like the caret is indented even through
            // the column is zero. Our code suggestion be based on a zero column
            // position and include the needed indentation.
            //
            // To make the display of the suggestion look correct to the user
            // we need to trim any preceding indentation. But only for the display
            // value. We want to insert with the correct indentation.
            if (_textView.Caret.InVirtualSpace && column == 0)
                displaySuggestion = displaySuggestion.TrimStart();

            var edits = new List<ProposedEdit>
            {
                new ProposedEdit(
                    _textView.GetTextElementSpan(_textView.Caret.Position.BufferPosition),
                    displaySuggestion)
            };

            var proposals = new List<Proposal> {
                new Proposal(
                    description: null,
                    edits: edits,
                    caret: caret,
                    completionState: completionState,
                    flags: flags,
                    // The default commit action must be overriden so 
                    // that the committed suggestion can be different
                    // from the displayed suggestion. This will also
                    // be needed to add metrics on suggestion acceptance.
                    commitAction: () => { CommitSuggestion(suggestion); return true; },
                    proposalId: null,
                    acceptText: null,
                    previewText: null,
                    nextText: null,
                    scope: null)
            };

            return new ProposalCollection(_sourceName, proposals);
        }

        /// <summary>
        /// Call the Code Suggestion API and return a suggestion.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>Returns a suggestion or null</returns>
        private async Task<(string completion, string error)> GetCodeSuggestionAsync(CancellationToken cancellationToken)
        {
            var bufferPosition = _textView.Caret.Position.BufferPosition;
            var line = bufferPosition.GetContainingLine();
            var lineNumber = line.LineNumber;
            var column = bufferPosition.Position - line.Start.Position;

            if (cancellationToken.IsCancellationRequested)
                return (null, null);

            var ret = await _lsClient.SendTextDocumentCompletionAsync(
                _relativeFilePath,
                (uint)lineNumber,
                (uint)column,
                cancellationToken);

            if (ret.Completions == null || ret.Completions.Length == 0)
                return (null, ret.Error);

            return (ret.Completions[0].insertText, ret.Error);
        }

        /// <summary>
        /// Get solution name and path
        /// </summary>
        /// <param name="solutionName"></param>
        /// <param name="solutionPath"></param>
        /// <returns>Returns true on success or false on failure.</returns>
        private bool GetSolutionNameAndPath(out string solutionName, out string solutionPath)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            var dte2 = (DTE2)Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(SDTE));
            if (dte2 == null)
            {
                solutionName = null;
                solutionPath = null;
                return false;
            }

            solutionName = dte2.Solution.FullName;
            solutionPath = Path.GetDirectoryName(dte2.Solution.FileName);

            if(solutionName.ToLower().EndsWith(".sln"))
                solutionName = Path.GetFileNameWithoutExtension(solutionName);

            return solutionName != null && solutionPath != null;
        }

        /// <summary>
        /// Get the solutions path prefix. The solution folder
        /// name is left in, so all files have at least one directory in their path.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns>Returns a path prefix or null.</returns>
        private string GetContainingSolutionPathPrefix(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return null;

            // Remove solution filename
            var pathPrefix = Path.GetDirectoryName(_solutionPath);
            return pathPrefix;
        }

#pragma warning disable CS1998
        public override async Task DisposeAsync()
        {
            if (_disposed)
                return;

            _lsClient = null;
            _disposed = true;
        }
#pragma warning restore CS1998
    }
}
