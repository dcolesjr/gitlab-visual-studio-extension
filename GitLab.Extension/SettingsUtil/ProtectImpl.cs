﻿using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;

namespace GitLab.Extension.SettingsUtil
{
    public class ProtectImpl : ISettingsProtect
    {
        public string Protect(string data)
        {
            try
            {
                var dataAsBytes = UTF8Encoding.UTF8.GetBytes(data);
                var protectedAsBytes = ProtectedData.Protect(dataAsBytes, null, DataProtectionScope.CurrentUser);
                return System.Convert.ToBase64String(protectedAsBytes);
            }
            catch (CryptographicException e)
            {
                Trace.WriteLine($"Settings: Protect exception {e}");
                return string.Empty;
            }
        }

        public string Unprotect(string protectedData)
        {
            try
            {
                var protectedDataAsBytes = System.Convert.FromBase64String(protectedData);
                var dataAsBytes = ProtectedData.Unprotect(protectedDataAsBytes, null, DataProtectionScope.CurrentUser);
                return UTF8Encoding.UTF8.GetString(dataAsBytes);
            }
            catch (CryptographicException e)
            {
                Trace.WriteLine($"Settings: Unprotect exception {e}");
                return string.Empty;
            }
        }
    }
}
