﻿
namespace GitLab.Extension.SettingsUtil
{
    public interface ISettingsProtect
    {
        /// <summary>
        /// Protect (encrypt) data using the Windows Data Protection API (DAPI)
        /// </summary>
        /// <param name="data">Data to be encrypted</param>
        /// <returns>Encrypted value or string.Empty on error.</returns>
        string Protect(string data);

        /// <summary>
        /// Unprotect (decrypt) data using the Windows Data Protection API (DAPI)
        /// </summary>
        /// <param name="protectedData"></param>
        /// <returns>Encrypted value or string.Empty on error.</returns>
        string Unprotect(string protectedData);
    }
}
