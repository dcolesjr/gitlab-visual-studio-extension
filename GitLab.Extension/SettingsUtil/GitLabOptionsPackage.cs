﻿using Microsoft.VisualStudio.Shell;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Threading;
using Task = System.Threading.Tasks.Task;
using System.ComponentModel;

namespace GitLab.Extension.SettingsUtil
{
    /// <summary>
    /// This is the class that implements the package exposed by this assembly.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The minimum requirement for a class to be considered a valid package for Visual Studio
    /// is to implement the IVsPackage interface and register itself with the shell.
    /// This package uses the helper classes defined inside the Managed Package Framework (MPF)
    /// to do it: it derives from the Package class that provides the implementation of the
    /// IVsPackage interface and uses the registration attributes defined in the framework to
    /// register itself and its components with the shell. These attributes tell the pkgdef creation
    /// utility what data to put into .pkgdef file.
    /// </para>
    /// <para>
    /// To get loaded into VS, the package must be referred by &lt;Asset Type="Microsoft.VisualStudio.VsPackage" ...&gt; in .vsixmanifest file.
    /// </para>
    /// </remarks>
    [PackageRegistration(UseManagedResourcesOnly = true, AllowsBackgroundLoading = true)]
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [Guid(GitLabOptionsPackage.PackageGuidString)]
    [ProvideOptionPage(typeof(OptionPageGrid), "GitLab", "General", 0, 0, true)]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
    public sealed class GitLabOptionsPackage : AsyncPackage
    {
        /// <summary>
        /// MyToolsOptionsPackage GUID string.
        /// </summary>
        public const string PackageGuidString = "8fc714ec-e5ad-47d0-99a2-9545ecb7923f";

        /// <summary>
        /// Initializes a new instance of the <see cref="GitLabOptionsPackage"/> class.
        /// </summary>
        public GitLabOptionsPackage()
        {
            // Inside this method you can place any initialization code that does not require
            // any Visual Studio service because at this point the package object is created but
            // not sited yet inside Visual Studio environment. The place to do all the other
            // initialization is the Initialize method.
        }
    }

    public class OptionPageGrid : DialogPage
    {
        [Category("GitLab")]
        [DisplayName("GitLab URL")]
        [Description("GitLab URL. For SaaS users, 'https://gitlab.com'.")]
        public string GitLabUrl { get; set; }

        [Category("GitLab")]
        [DisplayName("Access Token")]
        [PasswordPropertyText(true)]
        [Description("Personal access token with read_api and read_user permissions.")]
        public string AccessToken { get; set; }

        public override void LoadSettingsFromStorage()
        {
            base.LoadSettingsFromStorage();

            GitLabUrl = Settings.Instance.GitLabUrl;
            AccessToken = Settings.Instance.GitLabAccessToken;
        }

        public override void SaveSettingsToStorage()
        {
            base.SaveSettingsToStorage();

            Settings.Instance.StartBatchSettingsUpdate();

            Settings.Instance.GitLabUrl = GitLabUrl;
            Settings.Instance.GitLabAccessToken = AccessToken;
            
            Settings.Instance.StopBatchSettingsUpdate();
        }
    }
}
