﻿using Microsoft.Win32;
using System;
using System.Diagnostics;

namespace GitLab.Extension.SettingsUtil
{
    internal class RegistryStorage : ISettingsStorage
    {
        public ISettingsProtect Protect;

        public RegistryStorage(ISettingsProtect protect)
        {
            Protect = protect;
        }

        public void Load(Settings settings)
        {
            try
            {
                var key = Registry.CurrentUser.OpenSubKey($"Software\\{Settings.ApplicationName}", true);
                if (key == null)
                {
                    if (LoadLegacyPoCRegistry(settings))
                    {
                        // Save settings to current key
                        Save(settings);

                        try
                        {
                            // Delete old registry key
                            Registry.CurrentUser.DeleteSubKey($"Software\\{Settings.ApplicationNamePoC}");
                        }
                        catch
                        {
                            // Ignore exceptions deleting old registry key
                        }
                    }
                    else
                    {
                        settings.GitlabAccessTokenValue = string.Empty;
                    }

                    return;
                }

                try
                {
                    var protectedAccessToken = (string)key.GetValue(Settings.GitLabAccessTokenKey, null);
                    if (!string.IsNullOrEmpty(protectedAccessToken))
                    {
                        settings.GitlabAccessTokenValue = Protect.Unprotect(protectedAccessToken);
                    }
                    else
                    {
                        settings.GitlabAccessTokenValue = string.Empty;
                    }

                    int value = (int)key.GetValue(Settings.IsCodeSuggestionsEnabledKey, 1);
                    settings.IsCodeSuggestionsEnabledValue = value == 1;

                    settings.GitLabUrlValue = (string)key.GetValue(Settings.GitLabUrlKey, string.Empty);
                }
                finally
                {
                    key.Dispose();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine($"Settings.LoadRegistry(): Exception: {ex}");
                if (ex.InnerException != null)
                    Trace.WriteLine($"Settings.LoadRegistry(): InnerException: {ex.InnerException}");
                throw;
            }
        }

        public void Save(Settings settings)
        {
            var key = Registry.CurrentUser.OpenSubKey($"Software\\{Settings.ApplicationName}", true);
            if (key == null)
            {
                using (var softwareKey = Registry.CurrentUser.OpenSubKey($"Software", true))
                {
                    key = softwareKey.CreateSubKey(Settings.ApplicationName);
                }
            }

            try
            {
                key.SetValue(
                    Settings.GitLabUrlKey,
                    settings.GitLabUrlValue,
                    RegistryValueKind.String);
                key.SetValue(
                    Settings.GitLabAccessTokenKey,
                    Protect.Protect(settings.GitlabAccessTokenValue),
                    RegistryValueKind.String);
                key.SetValue(
                    Settings.IsCodeSuggestionsEnabledKey, 
                    settings.IsCodeSuggestionsEnabledValue? 1 : 0, RegistryValueKind.DWord);
            }
            finally
            {
                key.Dispose();
            }
        }

        private bool LoadLegacyPoCRegistry(Settings settings)
        {
            try
            {
                var key = Registry.CurrentUser.OpenSubKey($"Software\\{Settings.ApplicationNamePoC}", true);
                if (key == null)
                {
                    settings.GitlabAccessTokenValue = string.Empty;
                    return false;
                }

                try
                {
                    var protectedAccessToken = (string)key.GetValue(Settings.GitLabAccessTokenKey, null);
                    if (!string.IsNullOrEmpty(protectedAccessToken))
                    {
                        settings.GitlabAccessTokenValue = Protect.Unprotect(protectedAccessToken);
                    }
                    else
                    {
                        settings.GitlabAccessTokenValue = string.Empty;
                    }

                    int value = (int)key.GetValue(Settings.IsCodeSuggestionsEnabledKey, 1);
                    settings.IsCodeSuggestionsEnabledValue = value == 1;

                    return true;
                }
                finally
                {
                    key.Dispose();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine($"Settings.LoadLegacyPoCRegistry(): Exception: {ex}");
                if (ex.InnerException != null)
                    Trace.WriteLine($"Settings.LoadLegacyPoCRegistry(): InnerException: {ex.InnerException}");
                throw;
            }
        }

    }
}
