﻿using System;

namespace GitLab.Extension.SettingsUtil
{
    public interface ISettings
    {
        /// <summary>
        /// Call before setting multiple settings. This
        /// prevents the SettingsChangedEvent from being
        /// triggered on each individual settings change.
        /// </summary>
        void StartBatchSettingsUpdate();

        /// <summary>
        /// Called to stop batch mode and send a 
        /// SettingsChangedEvent.
        /// </summary>
        void StopBatchSettingsUpdate();

        /// <summary>
        /// GitLab Access Token
        /// </summary>
        string GitLabAccessToken { get; set; }

        /// <summary>
        /// Is code suggestions enabled
        /// </summary>
        bool IsCodeSuggestionsEnabled { get; set; }

        /// <summary>
        /// GitLab URL
        /// </summary>
        string GitLabUrl { get; set; }

        /// <summary>
        /// Do we have a valid looking configuration
        /// </summary>
        bool Configured { get; }

        /// <summary>
        /// Event triggered when settings change. The event args
        /// is an instance of Settings.SettingsEventArgs and contains
        /// the key for the setting that changed.
        /// </summary>
        event EventHandler SettingsChangedEvent;
    }
}
