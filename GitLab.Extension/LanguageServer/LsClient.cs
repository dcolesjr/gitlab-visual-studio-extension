﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using StreamJsonRpc;
using System.Diagnostics;
using GitLab.Extension.LanguageServer.Models;
using System.Threading;
using System.Reflection;
using GitLab.Extension.SettingsUtil;
using System.Runtime.InteropServices;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Code suggestions language server client.
    /// </summary>
    /// <remarks>
    /// Once the client is connected to a langauge server,
    /// it will automatically reconnect using an exponential backoff
    /// if the connection is lost.
    /// 
    /// Reconnecting means stopping/starting the language server
    /// in addition to our rpc connection.
    /// </remarks>
    internal class LsClient : IDisposable, IAsyncDisposable
    {
        private readonly ExponentialBackoffHelper _backoffHelper;
        private readonly string _solutionName;
        private readonly string _solutionPath;

        /// <summary>
        /// TCP connection to the language server
        /// </summary>
        private TcpClient _tcpClient;
        /// <summary>
        /// JsonRPC client connected to language server
        /// </summary>
        private JsonRpc _rpc;
        /// <summary>
        /// Prefix for Trace.WriteLine calls
        /// </summary>
        private string _tracePrefix = "LsClient(0):";
        /// <summary>
        /// Langauge server process manager instance.
        /// None-null indicates we have started a language server
        /// process.
        /// </summary>
        private LsProcessManager _lsProcessManager;
        /// <summary>
        /// The lanaguage server port number
        /// </summary>
        private int _lsPort = -1;
        /// <summary>
        /// Our we currently connected to a language server
        /// </summary>
        private bool _rpcConnected = false;
        /// <summary>
        /// Set after we have cleaned up all instance resources
        /// </summary>
        private bool _disposed = false;
        /// <summary>
        /// Set when we are trying freeing resources
        /// </summary>
        private bool _disposing = false;

        /// <summary>
        /// Are we connected to a language server process?
        /// </summary>
        public bool IsConnected {  get {  return _rpcConnected; } } 

        /// <summary>
        /// DO NOT INSTANTIATE DIRECTLY, USE LsClientManager!!!
        /// </summary>
        /// <param name="solutionName"></param>
        /// <param name="solutionPath"></param>
        public LsClient(string solutionName, string solutionPath)
        {
            _backoffHelper = new ExponentialBackoffHelper();
            _solutionName = solutionName;
            _solutionPath = solutionPath;
        }

        /// <summary>
        /// Start language server
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <returns>Returns language server port, or -1 on error.</returns>
        private async Task<int> StartLanguageServerAsync(string solutionPath)
        {
            if (_lsProcessManager != null)
                return _lsPort;

            _lsPort = -1;

            await Task.Run(() =>
            {
                try
                {
                    _lsProcessManager = LsProcessManager.Instance;
                    _lsProcessManager.StartLanguageServer(
                        solutionPath,
                        Settings.Instance.GitLabUrl,
                        Settings.Instance.GitLabAccessToken,
                        out _lsPort);
                }
                catch(Exception ex)
                {
                    Trace.WriteLine($"{_tracePrefix} StartLanguageServerAsync: Exception: {ex}");
                    if(ex.InnerException != null)
                        Trace.WriteLine($"{_tracePrefix} StartLanguageServerAsync: innerException: {ex.InnerException}");
                }
            });

            Settings.Instance.SettingsChangedEvent += SettingsChangedEvent;

            return _lsPort;
        }

        /// <summary>
        /// Called when the settings change to restart
        /// our language server with latest settings.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsChangedEvent(object sender, EventArgs e)
        {
            if (_disposed)
                return;

            // Only restart when access token has changed
            var settingsChangedEventArgs = e as Settings.SettingsEventArgs;
            if (settingsChangedEventArgs.ChangedSettingKey != Settings.GitLabAccessTokenKey
                && settingsChangedEventArgs.ChangedSettingKey != Settings.ApplicationName)
            {
                return;
            }

            // No need to block waiting for this to happen
            _ = ConnectAsync(true);
        }

        /// <summary>
        /// Cleanup resources before restarting
        /// </summary>
        private async Task RestartCleanupAsync()
        {
            _lsPort = -1;

            if (_rpc != null)
                _rpc.Disconnected -= rpc_Disconnected;

            _rpc = null;
            _tcpClient?.Dispose();

            if(_lsProcessManager != null)
                await _lsProcessManager.StopLanguageServerAsync(_solutionPath);

            _lsProcessManager = null;
        }

        /// <summary>
        /// Connect to a language server. If a language server doesn't
        /// exist one will be created.
        /// </summary>
        /// <returns>True on success, false on failure</returns>
        public async Task<bool> ConnectAsync()
        {
            Trace.WriteLine($"{_tracePrefix} ConnectAsync: Name: {_solutionName}, Path: {_solutionPath}");
            return await ConnectAsync(false);
        }

        /// <summary>
        /// Connect to a language server. If a language server doesn't
        /// exist one will be created.
        /// </summary>
        /// <param name="reconnect">Reconnect to the language server.</param>
        /// <returns>True on success, false on failure</returns>
        private async Task<bool> ConnectAsync(bool reconnect = false)
        {
            try
            {
                if (_disposed)
                    throw new ObjectDisposedException(nameof(LsClient));

                if (_disposing)
                    return false;

                // Make sure we have a valid configuration
                if (!Settings.Instance.Configured)
                    return false;

                if (!reconnect && _tcpClient != null)
                    return false;

                // Use our backoff helper to make sure we don't
                // keep restarting in a tight loop.
                // There is no delay on first call
                if (!_backoffHelper.ShouldRetry)
                    return false;
                await _backoffHelper.DelayAsync();

                // If we are reconnecting cleanup existing resources
                if (reconnect)
                    await RestartCleanupAsync();

                // Start a license server instance if we haven't already
                await StartLanguageServerAsync(_solutionPath);
                if (_lsPort == -1)
                    return false;

                _tracePrefix = $"LsClient({_lsPort}):";

                // Connect to our license server
                _tcpClient = new TcpClient();
                await _tcpClient.ConnectAsync(IPAddress.Loopback, _lsPort);
                var stream = _tcpClient.GetStream();

                // Attach to the network stream for RPC
                _rpc = JsonRpc.Attach(stream, new LsClientRpc());
                _rpc.Disconnected += rpc_Disconnected;

                // Send the LSP 'initialize' message
                var initializationResult = await SendInitializeAsync(_solutionName, _solutionPath);

                _rpcConnected = true;
                return true;
            }
            catch(Exception ex)
            {
                // TODO - Log errors/display to user somehow

                Trace.WriteLine($"{_tracePrefix} ConnectAsync exception: {ex}");
                if (ex.InnerException != null)
                    Trace.WriteLine($"{_tracePrefix} ConnectAsync inner exception: {ex.InnerException}");

                return false;
            }
        }

        /// <summary>
        /// Called when we loose our connection to the license server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void rpc_Disconnected(object sender, JsonRpcDisconnectedEventArgs e)
        {
            _rpcConnected = false;
            _tracePrefix = "LsClient(0):";

            if (_disposed || _disposing)
            {
                Trace.WriteLine($"{_tracePrefix} rpc_Disconnected: _disposing = true");
                return;
            }

            Trace.WriteLine($"{_tracePrefix} rpc_Disconnected: Restarting");

            // No need to block waiting for this to happen
            _ = ConnectAsync(true);
        }

        /// <summary>
        /// Check if we are connected to language server
        /// </summary>
        /// <returns></returns>
        private bool IsRpcConnected()
        {
            return _rpcConnected && !_disposed && !_disposing;
        }

        /// <summary>
        /// Create a "file:///" url from a path
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string FilePathToUri(string filePath)
        {
            return $"file://{filePath.Replace(System.IO.Path.DirectorySeparatorChar, '/')}";
        }

        /// <summary>
        /// Send the LSP 'initialize' message
        /// </summary>
        /// <param name="solutionName"></param>
        /// <param name="solutionPath"></param>
        /// <returns></returns>
        private async Task<InitializeResult> SendInitializeAsync(string solutionName, string solutionPath)
        {
            var clientCapabilities = new ClientCapabilities()
            {
                textDocument = new ClientCapabilities.TextDocumentClientCapabilities()
                {
                    completion = new ClientCapabilities.TextDocumentClientCapabilities.CompletionClientCapabilities()
                    {
                        competionItem = new ClientCapabilities.TextDocumentClientCapabilities.CompletionClientCapabilities.CompletionItemClientCapabilities()
                        {
                            documentationFormat = MarkupKind.PlainText,
                            insertReplaceSupport = false,
                        },
                        completionItemKind = new ClientCapabilities.TextDocumentClientCapabilities.CompletionClientCapabilities.CompletionItemKindClientCapabilities()
                        {
                            valueSet = new CompletionItemKind[] { CompletionItemKind.Text }
                        },
                        contextSupport = false,
                        insertTextMode = InsertTextMode.AdjustIndentation,
                    },
                    codeAction = new ClientCapabilities.TextDocumentClientCapabilities.CodeActionClientCapabilities()
                    {
                    },
                    synchronization = new ClientCapabilities.TextDocumentClientCapabilities.TextDocumentSyncClientCapabilities()
                    {
                    },
                },
            };

            var initializeParams = new InitializeParams
            {
                processId = Process.GetCurrentProcess().Id,
                capabilities = clientCapabilities,
                clientInfo = new InitializeParams.ClientInfo()
                {
                    name = "gl-visual-studio-extension",
                    version = $"{Assembly.GetExecutingAssembly().GetName().Version}; arch:{RuntimeInformation.OSArchitecture}; vs:{VSVersion.FullVersion}; os:{VSVersion.OSVersion}",
                },
                workspaceFolders = new WorkspaceFolder[]
                {
                        new WorkspaceFolder()
                        {
                            name = solutionName,
                            // Uri will automatically convert a file path (c:\...) to
                            // a uri (file:///c:/...)
                            uri = FilePathToUri(solutionPath),
                        },
                },
            };

            var initializeResult = await _rpc?.InvokeWithParameterObjectAsync<InitializeResult>(
                "initialize", initializeParams);

            return initializeResult;
        }

        /// <summary>
        /// Send the 'textDocument/didOpen' notification message
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="version"></param>
        /// <param name="text"></param>
        /// <returns>True if sent, false if not sent</returns>
        public async Task<bool> SendTextDocumentDidOpenAsync(string filePath, int version, string text)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                var didOpenParams = new DidOpenTextDocumentNotificationParams()
                {
                    textDocument = new TextDocumentItem
                    { 
                        uri = FilePathToUri(filePath),
                        version = version,
                        text = text,
                    },
                };

                Trace.WriteLine($"{_tracePrefix} SendTextDocumentDidOpenAsync(\"{filePath}\", {version}, text.Length:{text.Length})");
                await _rpc?.NotifyWithParameterObjectAsync("textDocument/didOpen", didOpenParams);
                return true;
            }
            catch (Exception e)
            {
                Trace.WriteLine($"{_tracePrefix} Exception sending textDocument/didOpen: {e})");
                return false;
            }
        }

        /// <summary>
        /// Send the LSP 'textDocument/didChange' notification message.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="version"></param>
        /// <param name="fileText"></param>
        /// <returns>True if sent, false if not sent</returns>
        public async Task<bool> SendTextDocumentDidChangeAsync(string filePath, int version, string fileText)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                var didChangeParams = new DidChangeTextDocumentNotificationParams()
                {
                    textDocument = new VersionedTextDocumentIdentifier()
                    {
                        uri = FilePathToUri(filePath),
                        version = version,
                    },
                    contentChanges = new TextDocumentContentChangeEventWhole[]
                    {
                        new TextDocumentContentChangeEventWhole()
                        {
                            text = fileText,
                        }
                    }
                };

                Trace.WriteLine($"{_tracePrefix} SendTextDocumentDidChangeAsync(\"{filePath}\", {version}, ...)");

                await _rpc?.NotifyWithParameterObjectAsync("textDocument/didChange", didChangeParams);
                return true;
            }
            catch(Exception e)
            {
                Trace.WriteLine($"{_tracePrefix} Exception sending textDocument/didChange: {e})");
                return false;
            }
        }

        /// <summary>
        /// Send the 'textDocument/completion' request message.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="line"></param>
        /// <param name="character"></param>
        /// <param name="token"></param>
        /// <returns>Array of CompletionItems or null.</returns>
        public async Task<(CompletionItem[] Completions, string Error)> SendTextDocumentCompletionAsync(
            string filePath, uint line, uint character, CancellationToken token)
        {
            try
            {
                if (!IsRpcConnected())
                    return (null, null);

                var completionPrams = new CompletionParams()
                {
                    textDocument = new TextDocumentIdentifier()
                    {
                        uri = FilePathToUri(filePath),
                    },
                    position = new Position()
                    {
                        line = line,
                        character = character,
                    },
                    context = new CompletionContext()
                    {
                        triggerKind = CompletionTriggerKind.Invoked,
                    }
                };

                Trace.WriteLine($"{_tracePrefix} SendTextDocumentCompletionAsync(\"{filePath}\", {line}, {character})");

                var ret = await _rpc?.InvokeWithParameterObjectAsync<CompletionItem[]>(
                    "textDocument/completion", completionPrams, token);

                if (ret == null)
                    Trace.WriteLine($"{_tracePrefix} SendTextDocumentCompletionAsync: Null response");

                if (ret.Length == 0)
                    Trace.WriteLine($"{_tracePrefix} SendTextDocumentCompletionAsync: 0 suggestions");

                return (ret, null);
            }
            catch (RemoteInvocationException ex)
            {
                var exString = ex.ToString();
                var errorMessage = string.Empty;

                if (exString.Contains("{\\\"error\\\":\\\"insufficient_scope\\\","))
                {
                    errorMessage = "Access token has insuffient scope (permission levels granted to the token). See the documentation for required scopes for the access token.";
                }
                else
                {
                    errorMessage = ex.Message;
                }

                Trace.WriteLine($"{_tracePrefix} Exception sending textDocument/completion: {ex})");
                return (null, errorMessage);
            }
            catch (Exception e)
            {
                Trace.WriteLine($"{_tracePrefix} Exception sending textDocument/completion: {e})");
                return (null, e.Message);
            }
        }

        /// <summary>
        /// Common dispose items that are not async. Called
        /// by both DisposeAsync and Dispose.
        /// </summary>
        private void CommonDispose()
        {
            Settings.Instance.SettingsChangedEvent -= SettingsChangedEvent;
        }

        public async ValueTask DisposeAsync()
        {
            if (_disposed)
                return;
            
            _disposing = true;

            Trace.WriteLine($"{_tracePrefix} DisposeAsync");
            CommonDispose();
            await RestartCleanupAsync();

            _disposed = true;

            GC.SuppressFinalize(this);
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposing = true;

            Trace.WriteLine($"{_tracePrefix} Dispose");

            CommonDispose();
#pragma warning disable VSTHRD002 // Avoid problematic synchronous waits
            RestartCleanupAsync().GetAwaiter().GetResult();
#pragma warning restore VSTHRD002 // Avoid problematic synchronous waits

            _disposed = true;

            GC.SuppressFinalize(this);
        }
    }
}
