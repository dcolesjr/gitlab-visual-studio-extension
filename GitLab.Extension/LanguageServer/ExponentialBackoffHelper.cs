﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Helper class for implementing an exponetial backoff
    /// </summary>
    internal class ExponentialBackoffHelper
    {
        private readonly int _delayMilliseconds;
        private readonly int _maxRetries;
        protected int _retries;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="maxRetries">Defaults to 15 which is a max 40 minute what between restarts.</param>
        /// <param name="delayMilliseconds">Defaults to 150 miliseconds</param>
        public ExponentialBackoffHelper(int maxRetries = 15, int delayMilliseconds = 150)
        {
            _delayMilliseconds = delayMilliseconds;
            _maxRetries = maxRetries;
            _retries = 0;
        }

        /// <summary>
        /// Should we retry?
        /// </summary>
        public bool ShouldRetry { get { return _retries < _maxRetries;  } }

        /// <summary>
        /// Get the current delay in milliseconds
        /// </summary>
        public int DelayMilliseconds 
        { 
            get
            {
                // No delay on first call;
                if (_retries == 0)
                    return 0;

                var pow = (int)Math.Pow(2, _retries);
                return _delayMilliseconds * pow;
            }
        }

        /// <summary>
        /// Perform delay for this retry
        /// </summary>
        /// <returns>Returns true if we delayed, false if past max retries</returns>
        public async Task<bool> DelayAsync()
        {
            if (_retries >= _maxRetries)
                return false;

            // No delay on first call;
            if (_retries == 0)
                return true;

            var delay = DelayMilliseconds;

            _retries++;
            
            await Task.Delay(delay);
            return true;
        }
    }
}
