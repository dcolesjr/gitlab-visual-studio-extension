﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLab.Extension.LanguageServer.Models
{
    internal class WorkDoneProgressParams
    {

    }



    internal class ClientCapabilities
    {
        internal class Workspace
        {
            internal class WorkspaceEditClientCapabilities
            {
                internal class ChangeAnnotationSupport
                {
                    /// <summary>
                    /// Whether the client groups edits with equal labels into tree nodes,
                    /// for instance all edits labelled with "Changes in Strings" would
                    /// be a tree node.
                    /// </summary>
                    public bool? groupsOnLabel;
                }

                /// <summary>
                /// The client supports versioned document changes in `WorkspaceEdit`s
                /// </summary>
                public bool? documentChanges;
                /// <summary>
                /// The resource operations the client supports. Clients should at least
                /// support 'create', 'rename' and 'delete' files and folders.
                /// </summary>
                public ResourceOperationKind[] resourceOperations;
                /// <summary>
                /// The failure handling strategy of a client if applying the workspace edit
                /// fails.
                /// </summary>
                public FailureHandlingKind failureHandling;
                /// <summary>
                /// Whether the client normalizes line endings to the client specific
                /// setting.
                /// If set to `true` the client will normalize line ending characters
                /// in a workspace edit to the client specific new line character(s).
                /// </summary>
                public bool? normalizesLineEndings;
                /// <summary>
                /// Whether the client in general supports change annotations on text edits,
                /// create file, rename file and delete file changes.
                /// </summary>
                public ChangeAnnotationSupport changeAnnotationSupport;
            }
            internal class DidChangeConfigurationClientCapabilities
            {
                /// <summary>
                /// The actual changed settings
                /// </summary>
                public object settings;
            }
            internal class DidChangeWatchedFilesClientCapabilities
            {
                /// <summary>
                /// Did change watched files notification supports dynamic registration.
                /// Please note that the current protocol doesn't support static
                /// configuration for file changes from the server side.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// Whether the client has support for relative patterns
                /// or not.
                /// </summary>
                public bool? relativePatternSupport;
            }
            internal class WorkspaceSymbolClientCapabilities
            {
                internal class SymbolKindField
                {
                    /// <summary>
                    /// The symbol kind values the client supports. When this
                    /// property exists the client also guarantees that it will 
                    /// handle values outside its set gracefully and falls back 
                    /// to a default value when unknown.
                    ///  
                    /// If this property is not present the client only supports
                    /// the symbol kinds from `File` to `Array` as defined in
                    /// the initial version of the protocol.
                    /// </summary>
                    public SymbolKind? valueSet;
                }
                internal class TagSupport
                {
                    /// <summary>
                    /// The tags supported by the client.
                    /// </summary>
                    public SymbolTag[] valueSet;
                }
                internal class ResolveSupport
                {
                    /// <summary>
                    /// The properties that a client can resolve lazily. Usually
                    /// `location.range`
                    /// </summary>
                    public string[] properties;
                }

                /// <summary>
                /// Symbol request supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// Specific capabilities for the `SymbolKind` in the `workspace/symbol`
                /// request.
                /// </summary>
                public SymbolKindField symbolFind;
                /// <summary>
                /// The client supports tags on `SymbolInformation` and `WorkspaceSymbol`.
                /// Clients supporting tags have to handle unknown tags gracefully.
                /// </summary>
                public TagSupport tagSupport;
                /// <summary>
                /// The client support partial workspace symbols. The client will send the
                /// request `workspaceSymbol/resolve` to the server to resolve additional
                /// properties.
                /// </summary>
                public ResolveSupport resolveSupport;
            }
            internal class ExecuteCommandClientCapabilities
            {
                /// <summary>
                /// Execute command supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
            }
            internal class SemanticTokenWorkspaceClientCapabilities
            {
                /// <summary>
                /// Whether the client implementation supports a refresh request sent from
                /// the server to the client.
                /// 
                /// Note that this event is global and will force the client to refresh all
                /// semantic tokens currently shown. It should be used with absolute care
                /// and is useful for situation where a server for example detect a project
                /// wide change that requires such a calculation.
                /// </summary>
                public bool? refreshSupport;
            }
            internal class CodeLensWorkspaceClientCapabilities
            {
                /// <summary>
                /// Whether the client implementation supports a refresh request sent from the
                /// server to the client.
                /// 
                /// Note that this event is global and will force the client to refresh all
                /// code lenses currently shown. It should be used with absolute care and is
                /// useful for situation where a server for example detect a project wide
                /// change that requires such a calculation.
                /// </summary>
                public bool? refreshSupport;
            }
            internal class FileOperations
            {
                /// <summary>
                /// Whether the client supports dynamic registration for file
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// The client has support for sending didCreateFiles notifications.
                /// </summary>
                public bool? didCreate;
                /// <summary>
                /// The client has support for sending willCreateFiles requests.
                /// </summary>
                public bool? willCreate;
                /// <summary>
                /// The client has support for sending didRenameFiles notifications.
                /// </summary>
                public bool? didRename;
                /// <summary>
                /// The client has support for sending willRenameFiles requests.
                /// </summary>
                public bool? willRename;
                /// <summary>
                /// The client has support for sending didDeleteFiles notifications.
                /// </summary>
                public bool? didDelete;
                /// <summary>
                /// The client has support for sending willDeleteFiles requests.
                /// </summary>
                public bool? willDelete;
            }
            internal class InlineValueWorkspaceClientCapabilities
            {
                /// <summary>
                /// Whether the client implementation supports a refresh request sent from
                /// the server to the client.
                /// 
                /// Note that this event is global and will force the client to refresh all
                /// inline values currently shown. It should be used with absolute care and
                /// is useful for situation where a server for example detect a project wide
                /// change that requires such a calculation.
                /// </summary>
                public bool? refreshSupport;
            }
            internal class InlayHintWorkspaceClientCapabilities
            {
                /// <summary>
                /// Whether the client implementation supports a refresh request sent from
                /// the server to the client.
                /// 
                /// Note that this event is global and will force the client to refresh all
                /// inlay hints currently shown. It should be used with absolute care and
                /// is useful for situation where a server for example detects a project wide
                /// change that requires such a calculation.
                /// </summary>
                public bool? refreshSupport;
            }
            internal class DiagnosticWorkspaceClientCapabilities
            {
                /// <summary>
                /// Whether the client implementation supports a refresh request sent from
                /// the server to the client.
                /// 
                /// Note that this event is global and will force the client to refresh all
                /// pulled diagnostics currently shown. It should be used with absolute care
                /// and is useful for situation where a server for example detects a project
                /// wide change that requires such a calculation.
                /// </summary>
                public bool? refreshSupport;
            }

            /// <summary>
            /// The client supports applying batch edits
            /// to the workspace by supporting the request
            /// 'workspace/applyEdit'
            /// </summary>
            public bool? applyEdit;
            /// <summary>
            /// Capabilities specific to `WorkspaceEdit`s
            /// </summary>
            public WorkspaceEditClientCapabilities workspaceEdit;
            /// <summary>
            /// Capabilities specific to the `workspace/didChangeConfiguration`
            /// notification.
            /// </summary>
            public DidChangeConfigurationClientCapabilities didChangeConfiguration;
            /// <summary>
            /// Capabilities specific to the `workspace/didChangeWatchedFiles`
            /// notification.
            /// </summary>
            public DidChangeWatchedFilesClientCapabilities didChangeWatchedFiles;
            /// <summary>
            /// Capabilities specific to the `workspace/symbol` request.
            /// </summary>
            public WorkspaceSymbolClientCapabilities symbol;
            /// <summary>
            /// Capabilities specific to the `workspace/executeCommand` request.
            /// </summary>
            public ExecuteCommandClientCapabilities executeCommand;
            /// <summary>
            /// The client has support for workspace folders.
            /// </summary>
            public bool? workspaceFolders;
            /// <summary>
            /// The client supports `workspace/configuration` requests.
            /// </summary>
            public bool? configuration;
            /// <summary>
            /// Capabilities specific to the semantic token requests scoped to the
            /// workspace.
            /// </summary>
            public SemanticTokenWorkspaceClientCapabilities semanticTokens;
            /// <summary>
            /// Capabilities specific to the code lens requests scoped to the
            /// workspace.
            /// </summary>
            public CodeLensWorkspaceClientCapabilities codeLens;
            /// <summary>
            /// The client has support for file requests/notifications.
            /// </summary>
            public FileOperations fileOperations;
            /// <summary>
            /// Client workspace capabilities specific to inline values.
            /// </summary>
            public InlineValueWorkspaceClientCapabilities inlineValue;
            /// <summary>
            /// Client workspace capabilities specific to inlay hints.
            /// </summary>
            public InlayHintWorkspaceClientCapabilities inlayHint;
            /// <summary>
            /// Client workspace capabilities specific to diagnostics.
            /// </summary>
            public DiagnosticWorkspaceClientCapabilities diagnostics;
        }
        internal class TextDocumentClientCapabilities
        {
            internal class TextDocumentSyncClientCapabilities
            {
                /// <summary>
                /// Whether text document synchronization supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// The client supports sending will save notifications.
                /// </summary>
                public bool? willSave;
                /// <summary>
                /// The client supports sending a will save request and
                /// waits for a response providing text edits which will
                /// be applied to the document before it is saved.
                /// </summary>
                public bool? willSaveWaitUtil;
                /// <summary>
                /// The client supports did save notifications.
                /// </summary>
                public bool? didSave;
            }
            internal class CompletionClientCapabilities
            {
                internal class CompletionItemClientCapabilities
                {
                    internal class TagSupportClientCapabilities
                    {
                        /// <summary>
                        /// The tags supported by the client.
                        /// </summary>
                        public CompletionItemTag[] valueSet;
                    }
                    internal class ResolveSupportClientCapabilities
                    {
                        /// <summary>
                        /// The properties that a client can resolve lazily.
                        /// </summary>
                        public string[] properties;
                    }
                    internal class InsertTextModeSupportClientCapabilities
                    {
                        public InsertTextMode[] valueSet;
                    }

                    /// <summary>
                    /// Client supports snippets as insert text.
                    /// 
                    /// A snippet can define tab stops and placeholders with `$1`, `$2`
                    /// and `${3:foo}`. `$0` defines the final tab stop, it defaults to
                    /// the end of the snippet.Placeholders with equal identifiers are
                    /// linked, that is typing in one will update others too.
                    /// </summary>
                    public bool? snippetSupport;
                    /// <summary>
                    /// Client supports commit characters on a completion item.
                    /// </summary>
                    public bool? commitCharactersSupport;
                    /// <summary>
                    /// Client supports the follow content formats for the documentation
                    /// property.The order describes the preferred format of the client.
                    /// </summary>
                    public MarkupKind documentationFormat;
                    /// <summary>
                    /// Client supports the deprecated property on a completion item.
                    /// </summary>
                    public bool? deprecatedSupport;
                    /// <summary>
                    /// Client supports the preselect property on a completion item.
                    /// </summary>
                    public bool? preselectSupport;
                    /// <summary>
                    /// Client supports the tag property on a completion item. Clients
                    /// supporting tags have to handle unknown tags gracefully.Clients
                    /// especially need to preserve unknown tags when sending a completion
                    /// item back to the server in a resolve call.
                    /// </summary>
                    public TagSupportClientCapabilities tagSupport;
                    /// <summary>
                    /// Client supports insert replace edit to control different behavior if
                    /// a completion item is inserted in the text or should replace text.
                    /// </summary>
                    public bool? insertReplaceSupport;
                    /// <summary>
                    /// Indicates which properties a client can resolve lazily on a
                    /// completion item.Before version 3.16.0 only the predefined properties
                    /// `documentation` and `detail` could be resolved lazily.
                    /// </summary>
                    public ResolveSupportClientCapabilities resolveSupport;
                    /// <summary>
                    /// The client supports the `insertTextMode` property on
                    /// a completion item to override the whitespace handling mode
                    /// as defined by the client(see `insertTextMode`).
                    /// </summary>
                    public InsertTextModeSupportClientCapabilities insertTextModeSupport;
                    /// <summary>
                    /// The client has support for completion item label
                    /// details(see also `CompletionItemLabelDetails`).
                    /// </summary>
                    public bool? labelDetailsSupport;
                }
                internal class CompletionItemKindClientCapabilities
                {
                    /// <summary>
                    /// The completion item kind values the client supports. When this
                    /// property exists the client also guarantees that it will
                    /// handle values outside its set gracefully and falls back
                    /// to a default value when unknown.
                    /// 
                    /// If this property is not present the client only supports
                    /// the completion items kinds from `Text` to `Reference` as defined in
                    /// the initial version of the protocol.
                    /// </summary>
                    public CompletionItemKind[] valueSet;
                }
                internal class CompletionListClientCapabilities
                {
                    /// <summary>
                    /// The client supports the following itemDefaults on
                    /// a completion list.
                    /// 
                    /// The value lists the supported property names of the
                    /// `CompletionList.itemDefaults` object. If omitted
                    /// no properties are supported.
                    /// </summary>
                    public string[] itemDefaults;
                }
                /// <summary>
                /// Whether completion supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// The client supports the following `CompletionItem` specific
                /// capabilities.
                /// </summary>
                public CompletionItemClientCapabilities competionItem;
                /// <summary>
                /// See docs on inner memebers
                /// </summary>
                public CompletionItemKindClientCapabilities completionItemKind;
                /// <summary>
                /// The client supports to send additional context information for a
                /// `textDocument/completion` request.
                /// </summary>
                public bool? contextSupport;
                /// <summary>
                /// The client's default when the completion item doesn't provide a
                /// `insertTextMode` property.
                /// </summary>
                public InsertTextMode? insertTextMode;
                /// <summary>
                /// The client supports the following `CompletionList` specific
                /// capabilities.
                /// </summary>
                public CompletionListClientCapabilities completionList;
            }
            internal class HoverClientCapabilities
            {
                /// <summary>
                /// Whether hover supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// Client supports the follow content formats if the content
                /// property refers to a `literal of type MarkupContent`.
                /// The order describes the preferred format of the client.
                /// </summary>
                public MarkupKind contentFormat;
            }
            internal class SignatureHelpClientCapabilities
            {
                internal class SignatureInformation
                {
                    internal class ParameterInformationClientCapabilities
                    {
                        /// <summary>
                        /// The client supports processing label offsets instead of a
                        /// simple label string.
                        /// </summary>
                        public bool? labelOffsetSupport;
                    }
                    /// <summary>
                    /// Client supports the follow content formats for the documentation
                    /// property.The order describes the preferred format of the client.
                    /// </summary>
                    public MarkupKind documentationFormat;
                    /// <summary>
                    /// Client capabilities specific to parameter information.
                    /// </summary>
                    public ParameterInformationClientCapabilities parameterInformation;
                    /// <summary>
                    /// The client supports the `activeParameter` property on
                    /// `SignatureInformation` literal.
                    /// </summary>
                    public bool? activeParameterSupport;
                }
                /// <summary>
                /// Whether signature help supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// The client supports the following `SignatureInformation`
                /// specific properties.
                /// </summary>
                public SignatureInformation signatureInformation;
                /// <summary>
                /// The client supports to send additional context information for a
                /// `textDocument/signatureHelp` request.A client that opts into
                /// contextSupport will also support the `retriggerCharacters` on
                /// `SignatureHelpOptions`.
                /// </summary>
                public bool? contextSupport;
            }
            internal class DeclarationClientCapabilities
            {
                /// <summary>
                /// Whether declaration supports dynamic registration. If this is set to
                /// `true` the client supports the new `DeclarationRegistrationOptions`
                /// return value for the corresponding server capability as well.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// The client supports additional metadata in the form of declaration links.
                /// </summary>
                public bool? linkSupport;
            }
            internal class DefinitionClientCapabilities
            {
                /// <summary>
                /// Whether definition supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// The client supports additional metadata in the form of definition links.
                /// </summary>
                public bool? linkSupport;
            }
            internal class TypeDefinitionClientCapabilities
            {
                /// <summary>
                /// Whether implementation supports dynamic registration. If this is set to
                /// `true` the client supports the new `TypeDefinitionRegistrationOptions`
                /// return value for the corresponding server capability as well.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// The client supports additional metadata in the form of definition links.
                /// </summary>
                public bool? linkSupport;
            }
            internal class ImplementationClientCapabilities
            {
                /// <summary>
                /// Whether implementation supports dynamic registration. If this is set to
                /// `true` the client supports the new `ImplementationRegistrationOptions`
                /// return value for the corresponding server capability as well.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// The client supports additional metadata in the form of definition links.
                /// </summary>
                public bool? linkSupport;
            }
            internal class ReferenceClientCapabilities
            {
                /// <summary>
                /// Whether references supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
            }
            internal class DocumentHighlightClientCapabilities
            {
                /// <summary>
                /// Whether document highlight supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
            }
            internal class DocumentSymbolClientCapabilities
            {
                internal class SymbolKindClientCapabilities
                {
                    /// <summary>
                    /// The symbol kind values the client supports. When this
                    /// property exists the client also guarantees that it will
                    /// handle values outside its set gracefully and falls back
                    /// to a default value when unknown.
                    /// 
                    /// If this property is not present the client only supports
                    /// the symbol kinds from `File` to `Array` as defined in
                    /// the initial version of the protocol.
                    /// </summary>
                    public SymbolKind[] valueSet;
                }
                internal class TagSupportClientCapabilities
                {
                    /// <summary>
                    /// The tags supported by the client.
                    /// </summary>
                    public SymbolTag[] valueSet;
                }

                /// <summary>
                /// Whether document symbol supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// Specific capabilities for the `SymbolKind` in the
                /// `textDocument/documentSymbol` request.
                /// </summary>
                public SymbolKindClientCapabilities symbolKind;
                /// <summary>
                /// The client supports hierarchical document symbols.
                /// </summary>
                public bool? hierarchicalDocumentSymbolSupport;
                /// <summary>
                /// The client supports tags on `SymbolInformation`. Tags are supported on
                /// `DocumentSymbol` if `hierarchicalDocumentSymbolSupport` is set to true.
                /// Clients supporting tags have to handle unknown tags gracefully.
                /// </summary>
                public TagSupportClientCapabilities tagSupport;
                /// <summary>
                /// The client supports an additional label presented in the UI when
                /// registering a document symbol provider.
                /// </summary>
                public bool? labelSupport;
            }
            internal class CodeActionClientCapabilities
            {
                internal class CodeActionLiteralSupportClientCapabilities
                {
                    internal class CodeActionKindClientCapabilities
                    {
                        /// <summary>
                        /// The code action kind values the client supports. When this
                        /// property exists the client also guarantees that it will
                        /// handle values outside its set gracefully and falls back
                        /// to a default value when unknown.
                        /// </summary>
                        public CodeActionKind[] valueSet;
                    }

                    /// <summary>
                    /// The code action kind is supported with the following value
                    /// set.
                    /// </summary>
                    public CodeActionKindClientCapabilities codeActionKind;
                }
                internal class ResolveSupportClientCapabilities
                {
                    /// <summary>
                    /// The properties that a client can resolve lazily.
                    /// </summary>
                    public string[] properties;
                }
                /// <summary>
                /// Whether code action supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// The client supports code action literals as a valid
                /// response of the `textDocument/codeAction` request.
                /// </summary>
                public CodeActionLiteralSupportClientCapabilities codeActionLiteralSupport;
                /// <summary>
                /// Whether code action supports the `isPreferred` property.
                /// </summary>
                public bool? isPreferredSupport;
                /// <summary>
                /// Whether code action supports the `disabled` property.
                /// </summary>
                public bool? disabledSupport;
                /// <summary>
                /// Whether code action supports the `data` property which is
                /// preserved between a `textDocument/codeAction` and a
                /// `codeAction/resolve` request.
                /// </summary>
                public bool? dataSupport;
                /// <summary>
                /// Whether the client supports resolving additional code action
                /// properties via a separate `codeAction/resolve` request.
                /// </summary>
                public ResolveSupportClientCapabilities resolveSupport;
                /// <summary>
                /// Whether the client honors the change annotations in
                /// text edits and resource operations returned via the
                /// `CodeAction#edit` property by for example presenting
                /// the workspace edit in the user interface and asking
                /// for confirmation.
                /// </summary>
                public bool? honorsChangeAnnotations;
            }
            internal class CodeLensClientCapabilities
            {
                /// <summary>
                /// Whether code lens supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
            }
            internal class DocumentLinkClientCapabilities
            {
                /// <summary>
                /// Whether document link supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
                /// <summary>
                /// Whether the client supports the `tooltip` property on `DocumentLink`.
                /// </summary>
                public bool? tooltipSupport;
            }
            internal class DocumentColorClientCapabilities
            {
                /// <summary>
                /// Whether document color supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
            }
            internal class DocumentFormattingClientCapabilities
            {
                /// <summary>
                /// Whether document formatting supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
            }
            internal class DocumentRangeFormattingClientCapabilities
            {
                /// <summary>
                /// Whether document formatting supports dynamic registration.
                /// </summary>
                public bool? dynamicRegistration;
            }
            public TextDocumentSyncClientCapabilities synchronization;
            public CompletionClientCapabilities completion;
            public HoverClientCapabilities hover;
            public SignatureHelpClientCapabilities signatureHelp;
            public DeclarationClientCapabilities declaration;
            public DefinitionClientCapabilities definition;
            public TypeDefinitionClientCapabilities typeDefinition;
            public ImplementationClientCapabilities implementation;
            public ReferenceClientCapabilities references;
            public DocumentHighlightClientCapabilities documentHighlight;
            public DocumentSymbolClientCapabilities documentSymbol;
            public CodeActionClientCapabilities codeAction;
            public CodeLensClientCapabilities codeLens;
            public DocumentLinkClientCapabilities documentLink;
            public DocumentColorClientCapabilities colorProvider;
            public DocumentFormattingClientCapabilities formatting;
            public DocumentRangeFormattingClientCapabilities rangeFormatting;

            // TODO: Add rest of the memebers
        }
        internal class NotebookDocumentClientCapabilities
        {

        }
        internal class WindowClientCapabilities
        {

        }
        internal class GeneralClientCapabilities
        {

        }

        /// <summary>
        /// Workspace specific client capabilities.
        /// </summary>
        public Workspace workspace;
        /// <summary>
        /// Text document specific client capabilities.
        /// </summary>
        public TextDocumentClientCapabilities textDocument;
        /// <summary>
        /// Capabilities specific to the notebook document support.
        /// </summary>
        public NotebookDocumentClientCapabilities notbookDocument;
        /// <summary>
        /// Window specific client capabilities.
        /// </summary>
        public WindowClientCapabilities window;
        /// <summary>
        /// General client capabilities.
        /// </summary>
        public GeneralClientCapabilities general;
        /// <summary>
        /// Experimental client capabilities.
        /// </summary>
        public object experimental;
    }

    internal class InitializeParams : WorkDoneProgressParams
    {
        internal class ClientInfo
        {
            /// <summary>
            /// The name of the client as defined by the client.
            /// </summary>
            public string name;
            /// <summary>
            /// The client's version as defined by the client.
            /// </summary>
            public string version;
        }

        /// <summary>
        /// The process Id of the parent process that started the server. Is null if
        /// the process has not been started by another process.If the parent
        /// process is not alive then the server should exit(see exit notification)
        /// its process.
        /// </summary>
        public int? processId;

        /// <summary>
        /// Information about the client
        /// </summary>
        public ClientInfo clientInfo;
        /// <summary>
        /// The locale the client is currently showing the user interface
        /// in. This must not necessarily be the locale of the operating
        /// system.
        /// 
        /// Uses IETF language tags as the value's syntax
        /// (See https://en.wikipedia.org/wiki/IETF_language_tag)
        /// </summary>
        public string locale;

        /// <summary>
        /// User provided initialization options.
        /// </summary>
        public object initializationOptions;

        /// <summary>
        /// The capabilities provided by the client(editor or tool)
        /// </summary>
        public ClientCapabilities capabilities;

        /// <summary>
        /// The workspace folders configured in the client when the server starts.
        /// This property is only available if the client supports workspace folders.
        /// It can be `null` if the client supports workspace folders but none are
        /// configured.
        /// </summary>
        public WorkspaceFolder[] workspaceFolders;
    }

    internal class ServerCapabilities
    {

    }

    internal class InitializeResult
    {
        internal class ServerInfo
        {
            public string name;
            public string version;
        }

        public ServerCapabilities capabilities;
        public ServerInfo serverInfo;
    }
}
