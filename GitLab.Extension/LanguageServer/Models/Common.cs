﻿using Microsoft.VisualStudio.RpcContracts.Commands;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace GitLab.Extension.LanguageServer.Models
{
    internal class WorkspaceFolder
    {
        /// <summary>
        /// The associated URI for this workspace folder.
        /// </summary>
        public string uri;
        /// <summary>
        /// The name of the workspace folder. Used to refer to this
        /// workspace folder in the user interface.
        /// </summary>
        public string name;
    }

    internal class ResourceOperationKind
    {

    }
    internal class FailureHandlingKind
    {

    }

    internal enum SymbolKind
    {
        File = 1,
        Module = 2,
        Namespace = 3,
        Package = 4,
        Class = 5,
        Method = 6,
        Property = 7,
        Field = 8,
        Constructor = 9,
        Enum = 10,
        Interface = 11,
        Function = 12,
        Variable = 13,
        Constant = 14,
        String = 15,
        Number = 16,
        Boolean = 17,
        Array = 18,
        Object = 19,
        Key = 20,
        Null = 21,
        EnumMember = 22,
        Struct = 23,
        Event = 24,
        Operator = 25,
        TypeParameter = 26,
    }

    /// <summary>
    /// Describes the content type that a client supports in various
    /// result literals like `Hover`, `ParameterInfo` or `CompletionItem`.
    /// 
    /// Please note that `MarkupKinds` must not start with a `$`. This kinds
    /// are reserved for internal usage.
    /// </summary>
    /// <remarks>
    /// Emulates an enum, but with string values.
    /// </remarks>
    internal class MarkupKind : IEqualityComparer
    {
        /// <summary>
        /// Plain text is supported as a content format
        /// </summary>
        public static MarkupKind PlainText { get { return new MarkupKind("plaintext", true); } }
        /// <summary>
        /// Markdown is supported as a content format
        /// </summary>
        public static MarkupKind Markdown { get { return new MarkupKind("markdown", true); } }

        /// <summary>
        /// All possible values for this object that mimicks an enum
        /// </summary>
        private static List<string> AllPossibleValues;

        /// <summary>
        /// Perfer casting this object to a string, or calling .ToString().
        /// </summary>
        public string Value { get; private set; }

        static MarkupKind()
        {
            AllPossibleValues = new List<string>
            {
                PlainText.Value,
                Markdown.Value,
            };
        }

        private MarkupKind(string value, bool skipValidation)
        {
            if (!skipValidation)
                throw new ArgumentException("Call the regular ctor when false",
                    nameof(skipValidation));
            Value = value;
        }

        /// <summary>
        /// Don't call this constructor, use MarkupKind.PlainText or MarkupKind.Markdown.
        /// </summary>
        /// <param name="value">One of 'markdown' or 'plaintext'</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException">Raised when value is not on of the expected values.</exception>
        public MarkupKind(string value)
        {
            if (value == null)
                throw new ArgumentNullException();
            if (!AllPossibleValues.Contains(value))
                throw new ArgumentException($"Expected on of 'plaintext' or 'markdown', but got {value}.");

            Value = value;
        }

        public new bool Equals(object x, object y)
        {
            if (x == null || y == null)
                return false;
            if (!(x is MarkupKind) || !(y is MarkupKind))
                return false;

            return (x as MarkupKind).Value == (y as MarkupKind).Value;
        }

        public int GetHashCode(object obj)
        {
            if (obj == null || !(obj is MarkupKind))
                throw new ArgumentException();

            return (obj as MarkupKind).Value.GetHashCode();
        }
    }

    internal enum CompletionItemTag
    {
        Deprecated = 1
    }

    internal enum SymbolTag
    {
        Depricated = 1
    }
    /// <summary>
    /// The kind of a completion entry.
    /// </summary>
    internal enum CompletionItemKind
    {
        Text = 1,
        Method = 2,
        Function = 3,
        Constructor = 4,
        Field = 5,
        Variable = 6,
        Class = 7,
        Interface = 8,
        Module = 9,
        Property = 10,
        Unit = 11,
        Value = 12,
        Enum = 13,
        Keyword = 14,
        Snippet = 15,
        Color = 16,
        File = 17,
        Reference = 18,
        Folder = 19,
        EnumMember = 20,
        Constant = 21,
        Struct = 22,
        Event = 23,
        Operator = 24,
        TypeParameter = 25,
    }

    /// <summary>
    ///  How whitespace and indentation is handled during completion
    ///  item insertion.
    /// </summary>
    internal enum InsertTextMode
    {
        /// <summary>
        /// The insertion or replace strings is taken as it is. If the
        /// value is multi line the lines below the cursor will be
        /// inserted using the indentation defined in the string value.
        /// The client will not apply any kind of adjustments to the
        /// string.
        /// </summary>
        AsIs = 1,
        /// <summary>
        /// The editor adjusts leading whitespace of new lines so that
        /// they match the indentation up to the cursor of the line for
        /// which the item is accepted.
        /// 
        /// Consider a line like this: <2tabs><cursor><3tabs>foo. Accepting a
        /// multi line completion item is indented using 2 tabs and all
        /// following lines inserted will be indented using 2 tabs as well.
        /// </summary>
        AdjustIndentation = 2,
    }

    /// <summary>
    /// The kind of a code action.
    /// 
    /// Kinds are a hierarchical list of identifiers separated by `.`,
    /// e.g. `"refactor.extract.function"`.
    /// 
    /// The set of kinds is open and client needs to announce the kinds it supports
    /// to the server during initialization.
    /// </summary>
    internal class CodeActionKind : IEqualityComparer
    {
        /// <summary>
        /// Empty kind.
        /// </summary>
        public static CodeActionKind Empty { get { return new CodeActionKind("", true); } }
        /// <summary>
        /// Base kind for quickfix actions: 'quickfix'.
        /// </summary>
        public static CodeActionKind QuickFix { get { return new CodeActionKind("quickFix", true); } }
        /// <summary>
        /// Base kind for refactoring actions: 'refactor'.
        /// </summary>
        public static CodeActionKind Refactor { get { return new CodeActionKind("refactor", true); } }
        /// <summary>
        /// Base kind for refactoring extraction actions: 'refactor.extract'.
        /// 
        /// Example extract actions:
        /// 
        /// - Extract method
        /// - Extract function
        /// - Extract variable
        /// - Extract interface from class
        /// - ...
        /// </summary>
        public static CodeActionKind RefactorExtract { get { return new CodeActionKind("refactor.extract", true); } }
        /// <summary>
        /// Base kind for refactoring inline actions: 'refactor.inline'.
        /// 
        /// Example inline actions:
        /// - Inline function
        /// - Inline variable
        /// - Inline constant
        /// - ...
        /// </summary>
        public static CodeActionKind RefactorInline { get { return new CodeActionKind("refactor.inline", true); } }
        /// <summary>
        /// Base kind for refactoring rewrite actions: 'refactor.rewrite'.
        /// 
        /// Example rewrite actions:
        /// 
        /// - Convert JavaScript function to class
        /// - Add or remove parameter
        /// - Encapsulate field
        /// - Make method static
        /// - Move method to base class
        /// - ...
        /// </summary>
        public static CodeActionKind RefactorRewrite { get { return new CodeActionKind("refactor.rewrite", true); } }
        /// <summary>
        /// Base kind for source actions: `source`.
        /// 
        /// Source code actions apply to the entire file.
        /// </summary>
        public static CodeActionKind Source { get { return new CodeActionKind("source", true); } }
        /// <summary>
        /// Base kind for an organize imports source action:
        /// `source.organizeImports`.
        /// </summary>
        public static CodeActionKind SourceOrganizeImports { get { return new CodeActionKind("source.organizeImports", true); } }
        /// <summary>
        /// Base kind for a 'fix all' source action: `source.fixAll`.
        /// 
        /// 'Fix all' actions automatically fix errors that have a clear fix that
        /// do not require user input.They should not suppress errors or perform
        /// unsafe fixes such as generating new types or classes.
        /// </summary>
        public static CodeActionKind SourceFixAll { get { return new CodeActionKind("source.fixAll", true); } }

        /// <summary>
        /// All possible values for this object that mimicks an enum
        /// </summary>
        private static List<string> AllPossibleValues;

        /// <summary>
        /// Perfer casting this object to a string, or calling .ToString().
        /// </summary>
        public string Value { get; private set; }

        static CodeActionKind()
        {
            AllPossibleValues = new List<string>
            {
                Empty.ToString(),
                QuickFix.ToString(),
                Refactor.ToString(),
                RefactorExtract.ToString(),
                RefactorInline.ToString(),
                RefactorRewrite.ToString(),
                Source.ToString(),
                SourceOrganizeImports.ToString(),
                SourceFixAll.ToString(),
            };
        }

        private CodeActionKind(string value, bool skipValidation)
        {
            if (!skipValidation)
                throw new ArgumentException("use regular ctor when skipValidation is false", nameof(skipValidation));
            Value = value;
        }

        /// <summary>
        /// Don't call me directly, instead use one of the static properties.
        /// This ctor exists for the RPC deserialization.
        /// </summary>
        /// <param name="value"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public CodeActionKind(string value)
        {
            if (value == null)
                throw new ArgumentNullException();

            if (!AllPossibleValues.Contains(value))
                throw new ArgumentException($"Expected one of \"{string.Join(",", AllPossibleValues)}\", but got \"{value}\".");

            Value = value;
        }

        public new bool Equals(object x, object y)
        {
            if (x == null || y == null)
                return false;
            if (!(x is CodeActionKind) || !(y is CodeActionKind))
                return false;

            return (x as CodeActionKind).Value == (y as CodeActionKind).Value;
        }

        public int GetHashCode(object obj)
        {
            if (obj == null || !(obj is CodeActionKind))
                throw new ArgumentException();

            return (obj as CodeActionKind).Value.GetHashCode();
        }

        public static implicit operator string(CodeActionKind v)
        {
            return v.Value;
        }

        public override string ToString()
        {
            return Value;
        }
    }

    internal enum CompletionTriggerKind
    {
        /// <summary>
        /// Completion was triggered by typing an identifier (24x7 code
        /// complete), manual invocation (e.g Ctrl+Space) or via API.
        /// </summary>
        Invoked = 1,
        /// <summary>
        /// Completion was triggered by a trigger character specified by
        /// the `triggerCharacters` properties of the
        /// `CompletionRegistrationOptions`.
        /// </summary>
        TriggerCharacter = 2,
        /// <summary>
        /// Completion was re-triggered as the current completion list is incomplete.
        /// </summary>
        TriggerForIncompleteCompletions = 3,
    }
    internal enum InsertTextFormat
    {
        /// <summary>
        /// The primary text to be inserted is treated as a plain string.
        /// </summary>
        PlainText = 1,
        /// <summary>
        /// The primary text to be inserted is treated as a snippet.
        /// 
        /// A snippet can define tab stops and placeholders with `$1`, `$2`
        /// and `${3:foo}`. `$0` defines the final tab stop, it defaults to
        /// the end of the snippet.Placeholders with equal identifiers are linked,
        /// that is typing in one will update others too.
        /// </summary>
        Snippet = 2,
    }
    internal enum DiagnosticSeverity
    {
        Error = 1,
        Warning = 2,
        Information = 3,
        Hint = 4,
    }
    internal class Location
    {
        public string uri;
        public Range range;
    }
}