﻿using Microsoft.VisualStudio.Language.Intellisense.AsyncCompletion.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace GitLab.Extension.LanguageServer.Models
{
    /// <summary>
    /// An item to transfer a text document from the client to the server.
    /// </summary>
    internal class TextDocumentItem
    {
        /// <summary>
        /// The text document's URI.
        /// </summary>
        public string uri;
        /// <summary>
        /// The text document's language identifier.
        /// </summary>
        public string languageId;
        /// <summary>
        /// The version number of this document (it will increase after each
        /// change, including undo/redo).
        /// </summary>
        public int version;
        /// <summary>
        /// The content of the opened text document.
        /// </summary>
        public string text;
    }
    internal class DidOpenTextDocumentNotificationParams
    {
        /// <summary>
        /// The document that was opened.
        /// </summary>
        public TextDocumentItem textDocument;
    }
    internal class DocumentFilter
    {
        /// <summary>
        /// A language id, like `typescript`.
        /// </summary>
        public string language;
        /// <summary>
        /// A Uri [scheme](#Uri.scheme), like `file` or `untitled`.
        /// </summary>
        public string scheme;
        /// <summary>
        /// A glob pattern, like `*.{ts,js}`.
        /// 
        /// Glob patterns can have the following syntax:
        /// - `*` to match one or more characters in a path segment
        /// - `?` to match on one character in a path segment
        /// - `**` to match any number of path segments, including none
        /// - `{}` to group sub patterns into an OR expression. (e.g. `**​/*.{ts,js}`
        /// matches all TypeScript and JavaScript files)
        /// - `[]` to declare a range of characters to match in a path segment
        /// (e.g., `example.[0-9]` to match on `example.0`, `example.1`, …)
        /// - `[!...]` to negate a range of characters to match in a path segment
        /// (e.g., `example.[!0-9]` to match on `example.a`, `example.b`, but
        /// not `example.0`)
        /// </summary>
        public string pattern;
    }
    internal class TextDocumentRegistrationOptions
    {
        /// <summary>
        /// A document selector to identify the scope of the registration. If set to
        /// null the document selector provided on the client side will be used.
        /// </summary>
        public DocumentFilter[] documentSelector;
    }
    internal class TextDocumentIdentifier
    {
        /// <summary>
        /// The text document's URI.
        /// </summary>
        public string uri;
    }
    internal class VersionedTextDocumentIdentifier : TextDocumentIdentifier
    {
        /// <summary>
        /// The version number of this document.
        /// 
        /// The version number of a document will increase after each change,
        /// including undo/redo.The number doesn't need to be consecutive.
        /// </summary>
        public int version;
    }
    public enum TextDocumentSyncKind
    {
        /// <summary>
        /// Documents should not be synced at all.
        /// </summary>
        None = 0,
        /// <summary>
        /// Documents are synced by always sending the full content
        /// of the document.
        /// </summary>
        Full = 1,
        /// <summary>
        /// Documents are synced by sending the full content on open.
        /// After that only incremental updates to the document are
        /// sent.
        /// </summary>
        Incremental = 2
    }
    internal class Position
    {
        /// <summary>
        /// Line position in a document (zero-based).
        /// </summary>
        public uint line;
        /// <summary>
        /// Character offset on a line in a document (zero-based). The meaning of this
        /// offset is determined by the negotiated `PositionEncodingKind`.
        /// 
        /// If the character value is greater than the line length it defaults back
        /// to the line length.
        /// </summary>
        public uint character;
    }
    internal class Range
    {
        /// <summary>
        /// The range's start position.
        /// </summary>
        public Position start;
        /// <summary>
        /// The range's end position.
        /// </summary>
        public Position end;
    }
    internal class TextDocumentContentChangeEvent
    {
        /// <summary>
        /// The range of the document that changed.
        /// </summary>
        public Range range;
        /// <summary>
        /// depricated
        /// </summary>
        public uint? rangeLength = null;
        /// <summary>
        /// The new text for the provided range.
        /// </summary>
        public string text;
    }
    internal class TextDocumentContentChangeEventWhole
    {
        /// <summary>
        /// The new text of the whole document.
        /// </summary>
        public string text;
    }
    internal class DidChangeTextDocumentNotificationParams : TextDocumentRegistrationOptions
    {
        /// <summary>
        /// The document that did change. The version number points
        /// to the version after all provided content changes have
        /// been applied.
        /// </summary>
        public VersionedTextDocumentIdentifier textDocument;
        /// <summary>
        /// The actual content changes. The content changes describe single state
        /// changes to the document.So if there are two content changes c1 (at
        /// array index 0) and c2(at array index 1) for a document in state S then
        /// c1 moves the document from S to S' and c2 from S' to S''. So c1 is
        /// computed on the state S and c2 is computed on the state S'.
        /// 
        /// To mirror the content of a document using change events use the following
        /// approach:
        /// - start with the same initial content
        /// - apply the 'textDocument/didChange' notifications in the order you
        /// receive them.
        /// - apply the `TextDocumentContentChangeEvent`s in a single notification
        /// in the order you receive them.
        /// </summary>
        public TextDocumentContentChangeEventWhole[] contentChanges;
    }
    internal class DidCloseTextDocumentNotificationParams
    {
        /// <summary>
        /// The document that was closed.
        /// </summary>
        public TextDocumentIdentifier textDocument;
    }
    internal class TextDocumentPositionParams
    {
        /// <summary>
        /// The text document.
        /// </summary>
        public TextDocumentIdentifier textDocument;
        /// <summary>
        /// The position inside the text document.
        /// </summary>
        public Position position;
    }
    internal class CompletionContext
    {
        /// <summary>
        /// How the completion was triggered.
        /// </summary>
        public CompletionTriggerKind triggerKind;
        /// <summary>
        /// The trigger character (a single character) that has trigger code
        /// complete.Is undefined if
        /// `triggerKind !== CompletionTriggerKind.TriggerCharacter`
        /// </summary>
        public string triggerCharacter;
    }
    internal class CompletionParams : WorkDoneProgressParams
    {
        /// <summary>
        /// The text document.
        /// </summary>
        public TextDocumentIdentifier textDocument;
        /// <summary>
        /// The position inside the text document.
        /// </summary>
        public Position position;

        /// <summary>
        /// An optional token that a server can use to report partial results (e.g.
        /// streaming) to the client.
        /// </summary>
        public int partialResultToken;

        /// <summary>
        /// The completion context. This is only available if the client specifies
        /// to send this using the client capability
        /// `completion.contextSupport === true`
        /// </summary>
        public CompletionContext context;
    }
    internal class CompletionItem
    {
        public string label;
        public CompletionItemKind kind;
        public CompletionItemTag[] tags;
        public string detail;
        public string documentation;
        public bool? deprecated;
        public bool? preselect;
        public string sortText;
        public string filterText;
        public string insertText;
        public InsertTextFormat? insertTextFormat;
        public InsertTextMode? insertTextMode;
    }
    internal class Diagnostic
    {
        internal class CodeDescription
        {
            /// <summary>
            /// An URI to open with more information about the diagnostic error.
            /// </summary>
            public string href;
        }
        internal class DiagnosticRelatedInformation
        {
            public Location location;
            public string message;
        }
        /// <summary>
        /// The range at which the message applies.
        /// </summary>
        public Range range;
        /// <summary>
        /// The diagnostic's severity. Can be omitted. If omitted it is up to the
        /// client to interpret diagnostics as error, warning, info or hint.
        /// </summary>
        public DiagnosticSeverity? severity;
        /// <summary>
        /// The diagnostic's code, which might appear in the user interface.
        /// </summary>
        public string code;
        /// <summary>
        /// An optional property to describe the error code.
        /// </summary>
        public CodeDescription codeDescription;
        /// <summary>
        /// A human-readable string describing the source of this
        /// diagnostic, e.g. 'typescript' or 'super lint'.
        /// </summary>
        public string source;
        /// <summary>
        /// The diagnostic's message.
        /// </summary>
        public string message;
        /// <summary>
        /// Additional metadata about the diagnostic.
        /// </summary>
        public int[] tags;
        /// <summary>
        /// An array of related diagnostic information, e.g. when symbol-names within
        /// a scope collide all definitions can be marked via this property.
        /// </summary>
        public DiagnosticRelatedInformation relatedInformation;
        /// <summary>
        ///  A data entry field that is preserved between a
        ///  `textDocument/publishDiagnostics` notification and
        ///  `textDocument/codeAction` request.
        /// </summary>
        public object data;
    }
    internal class PublishDiagnosticsNotificationParams
    {
        /// <summary>
        /// The URI for which diagnostic information is reported.
        /// </summary>
        public string uri;
        /// <summary>
        /// Optional the version number of the document the diagnostics are published
        /// for.
        /// </summary>
        public int? version;
        /// <summary>
        /// An array of diagnostic information items.
        /// </summary>
        public Diagnostic diagnostic;
    }
}
