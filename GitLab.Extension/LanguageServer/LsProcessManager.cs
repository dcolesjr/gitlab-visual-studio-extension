﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Threading;
using System.Collections.Concurrent;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Manage language server processes. 
    /// Singleton pattern with single instance as LsProcessManager.Instance.
    /// </summary>
    /// <remarks>
    /// LsClient handles restarting the process.
    /// </remarks>
    internal class LsProcessManager : IAsyncDisposable
    {
        /// <summary>
        /// All language server processes. Key is Solution Path.
        /// </summary>
        private static readonly ConcurrentDictionary<string, Process> Processes = new ConcurrentDictionary<string, Process>();
        /// <summary>
        /// Locks for language server process. Key is Solution Path.
        /// </summary>
        private static readonly ConcurrentDictionary<string, 
            (object lockLanguageServer, SpinLock stoppingLanguageServer)> ProcessLocks = new ConcurrentDictionary<string, (object lockLanguageServer, SpinLock stoppingLanguageServer)>();
        /// <summary>
        /// Port language server is listening on. Key is Solution Path.
        /// </summary>
        private static readonly ConcurrentDictionary<string, int> LanguageServerPorts = new ConcurrentDictionary<string, int>();

        /// <summary>
        /// License server executable name
        /// </summary>
        private const string LsExecutable = "gitlab-code-suggestions-language-server-windows-amd64.exe";

        private AutoResetEvent _startupBannerEvent = new AutoResetEvent(false);
        //private bool _seenStartupBanner = false;

        /// <summary>
        /// Singleton instance of LsProcessManager
        /// </summary>
        public static LsProcessManager Instance = new LsProcessManager();

        private bool _disposed;

        private LsProcessManager()
        {
        }

        /// <summary>
        /// Finalizer to make sure we cleanup all of our resources
        /// </summary>
        ~LsProcessManager()
        {
#pragma warning disable VSTHRD002 // Avoid problematic synchronous waits
            DisposeAsync().GetAwaiter().GetResult();
#pragma warning restore VSTHRD002 // Avoid problematic synchronous waits
        }

        /// <summary>
        /// Start a language server watching solutionPath.
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <param name="gitlabUrl"></param>
        /// <param name="gitlabToken"></param>
        /// <param name="port">Port the server is listening on</param>
        /// <returns>True if a language server was started, false on error, or if the server is already started.
        /// If the language server was already started, port is set to the listing port.</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        public bool StartLanguageServer(string solutionPath, string gitlabUrl, string gitlabToken, out int port)
        {
            if(_disposed)
                throw new ObjectDisposedException(nameof(LsProcessManager));

            // Get the lock for this solution, or create a new one
            if (!ProcessLocks.TryGetValue(solutionPath, out var locks))
            {
                locks = (new object(), new SpinLock());

                if(!ProcessLocks.TryAdd(solutionPath, locks))
                    ProcessLocks.TryGetValue(solutionPath, out locks);
            }

            lock (locks.lockLanguageServer)
            {
                var newProcess = false;

                if (!Processes.TryGetValue(solutionPath, out var lsProcess))
                {
                    lsProcess = new Process();
                    newProcess = true;

                    if (!Processes.TryAdd(solutionPath, lsProcess))
                        Processes.TryGetValue(solutionPath, out lsProcess);
                }
                else if(!lsProcess.HasExited)
                {
                    if(!LanguageServerPorts.TryGetValue(solutionPath, out port))
                    {
                        // This should never happen
                        throw new ApplicationException("LangugageServerPorts.TryGetValue failed. LsProcessManager dictionaries in unknown state.");
                    }

                    return false;
                }

                // If we have an existing lsProcess instance, dispose it before
                // starting a new language server
                if (!newProcess && lsProcess.HasExited)
                {
                    lsProcess.ErrorDataReceived -= _lsProcess_ErrorDataReceived;
                    lsProcess.OutputDataReceived -= _lsProcess_OutputDataReceived;
                    lsProcess.Dispose();

                    lsProcess = new Process();
                    Processes[solutionPath] = lsProcess;
                }

                var lsPath = GetLsExecutablePath();
                port = GetAvailablePort();
                LanguageServerPorts.TryAdd(solutionPath, port);

                var processStartInfo = new ProcessStartInfo
                {
                    CreateNoWindow = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    FileName = lsPath,
#if DEBUG
                    Arguments = "serve --debug"
#else
                    Arguments = "serve"
#endif
                };

                processStartInfo.Environment.Add("LANGSRV_NAME", solutionPath);
                processStartInfo.Environment.Add("LANGSRV_SRC_DIR", solutionPath);
                processStartInfo.Environment.Add("GITLAB_URL", gitlabUrl);
                processStartInfo.Environment.Add("LANGSRV_GITLAB_URL", gitlabUrl);
                processStartInfo.Environment.Add("LANGSRV_GITLAB_API_TOKEN", gitlabToken);
                processStartInfo.Environment.Add("LANGSRV_HOST", IPAddress.Loopback.ToString());
                processStartInfo.Environment.Add("LANGSRV_PORT", port.ToString());

                Trace.WriteLine($"LS(0): StartLanguageServer: ==== STARTING: {solutionPath} ====");

                lsProcess.StartInfo = processStartInfo;
                lsProcess.ErrorDataReceived += _lsProcess_ErrorDataReceived;
                lsProcess.OutputDataReceived += _lsProcess_OutputDataReceived;
                lsProcess.Start();
                lsProcess.BeginOutputReadLine();
                lsProcess.BeginErrorReadLine();

                try
                {
                    // Link our new child process to the visual studio process.
                    // This will guarantee that killing visual studio will also
                    // kill our children.
                    ChildProcessTracker.AddProcess(lsProcess);
                }
                catch
                {
                    // Ignore exceptions.
                    // If visual studio is running in compatibility mode with Win7,
                    // we might get an exception. Nothing we can do if an exception
                    // is raised.
                }

                return true;
            }
        }

        private void _lsProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            var process = (sender as Process);
            var solutionPath = process.StartInfo.Environment["LANGSRV_SRC_DIR"];
            if (!LanguageServerPorts.TryGetValue(solutionPath, out var lsPort))
                lsPort = -1;

            if (e.Data == null)
                return;

            foreach (var line in e.Data.SplitToLines())
                Trace.WriteLine($"LS({lsPort}): {line}");
        }

        private void _lsProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            var solutionPath = (sender as Process).StartInfo.Environment["LANGSRV_SRC_DIR"];
            if (!LanguageServerPorts.TryGetValue(solutionPath, out var lsPort))
                lsPort = -1;

            if (e.Data == null)
                return;

            foreach (var line in e.Data.SplitToLines())
                Trace.WriteLine($"LS({lsPort}): {line}");
        }

        /// <summary>
        /// Stop the language server associated with solutionPath.
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <returns>Returns true if a server was killed, false otherwise.</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        public async Task<bool> StopLanguageServerAsync(string solutionPath)
        {
            if (_disposed)
                throw new ObjectDisposedException(nameof(LsProcessManager));

            if (!ProcessLocks.TryGetValue(solutionPath, out var locks))
                return false;

            return await Task.Run<bool>(new Func<bool>(() =>
            {
                lock (locks.lockLanguageServer)
                {
                    var lockToken = false;
                    locks.stoppingLanguageServer.Enter(ref lockToken);
                    if (!lockToken)
                        return false;

                    try
                    {
                        if (!Processes.TryGetValue(solutionPath, out var lsProcess))
                            return true;

                        Trace.WriteLine($"LS(0): StopLanguageServerAsync({solutionPath})");

                        if (!lsProcess.HasExited)
                        {
                            lsProcess.Kill();
                            lsProcess.WaitForExit();
                        }

                        lsProcess.Dispose();
                        lsProcess = null;

                        LanguageServerPorts.TryRemove(solutionPath, out _);
                        Processes.TryRemove(solutionPath, out _);
                        ProcessLocks.TryRemove(solutionPath, out _);

                        return true;
                    }
                    finally
                    {
                        locks.stoppingLanguageServer.Exit();
                    }
                }
            }));
        }

        private string GetLsExecutablePath()
        {
            var extensionPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            var lsPath = Path.Combine(extensionPath, "Resources");
            lsPath = Path.Combine(lsPath, LsExecutable);

#if DEBUG
            // TODO convert this into a log statement
            if (!File.Exists(lsPath))
                MessageBox.Show($"executable not found in path.\n{extensionPath}");
#endif

            return lsPath;
        }

        /// <summary>
        /// Get an available (free) TCP port
        /// </summary>
        /// <returns>Returns an available (free) TCP port</returns>
        private int GetAvailablePort()
        {
            var loopbackEndpoint = new IPEndPoint(IPAddress.Loopback, port: 0);
            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                socket.Bind(loopbackEndpoint);
                return ((IPEndPoint)socket.LocalEndPoint).Port;
            }
        }

        protected async Task DisposeAsync(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                foreach(var solution in Processes.Keys.ToArray())
                {
                    await StopLanguageServerAsync(solution);
                }
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            _disposed = true;
        }

        /// <summary>
        /// Dispose of heald resources.
        /// </summary>
        /// <param name="solutionPath">Dispose of just resources related to solutionPath. When null, all resources are freed</param>
        /// <returns></returns>
        public async ValueTask DisposeAsync()
        {
            await DisposeAsync(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
