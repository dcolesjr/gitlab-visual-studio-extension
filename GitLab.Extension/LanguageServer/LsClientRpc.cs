﻿using GitLab.Extension.LanguageServer.Models;
using StreamJsonRpc;
using System.Diagnostics;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Handlers for LSP messages sent from the language server to the client.
    /// </summary>
    internal class LsClientRpc
    {
        [JsonRpcMethod("textDocument/publishDiagnostics")]
        public void textDocument_publishDiagnostics(PublishDiagnosticsNotificationParams pdParams)
        {
            // The language server reports errors this way.
            // Right now we don't have a way to handle errors
            // outside of logging it to the debug view.
            Trace.WriteLine($"LsClient(?): textDocument/publishDiagnostics: {pdParams.diagnostic.message}");
        }
    }
}
