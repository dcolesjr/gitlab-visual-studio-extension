﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Manage instances of LsClient
    /// </summary>
    internal class LsClientManager
    {
        /// <summary>
        /// Singleton instance of LsClientManager
        /// </summary>
        public static LsClientManager Instance;

        private readonly Dictionary<string, LsClient> _clients = new Dictionary<string, LsClient>();
        private readonly object _clientsLock = new object();

        static LsClientManager()
        {
            Instance = new LsClientManager();
        }

        ~LsClientManager()
        {
            foreach(var client in _clients.Values)
                client.Dispose();

            _clients.Clear();
        }

        /// <summary>
        /// Get an LsClient instance for a solution. A single LsClient instance
        /// is shared for eash unique solutionName.
        /// </summary>
        /// <param name="solutionName"></param>
        /// <param name="solutionPath"></param>
        /// <returns></returns>
        public LsClient GetClient(string solutionName, string solutionPath)
        {
            lock (_clientsLock)
            {
                if (!_clients.TryGetValue(solutionName, out LsClient client))
                {
                    client = new LsClient(solutionName, solutionPath);
                    _clients[solutionName] = client;
                }

                return client;
            }
        }

        /// <summary>
        /// Dispose an LsClient. This is only called by LsVisualStudioPackage.
        /// </summary>
        /// <param name="solutionName"></param>
        /// <returns></returns>
        public async Task DisposeClientAsync(string solutionName)
        {
            LsClient client = null;
            lock (_clientsLock)
            {

                if (!_clients.TryGetValue(solutionName, out client))
                    return;

                _clients.Remove(solutionName);
            }

            await client.DisposeAsync();
        }
    }
}
