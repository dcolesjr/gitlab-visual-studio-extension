﻿using EnvDTE80;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Events;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;
using GitLab.Extension.SettingsUtil;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Listen for solution open/closed events and start or cleanup langauge server processes.
    /// </summary>
    /// <remarks>
    /// These events don't get called for every solution open/closed event.
    /// But when they do it's useful for getting the language server started,
    /// or stopped correctly.
    /// </remarks>
    [PackageRegistration(UseManagedResourcesOnly = true, AllowsBackgroundLoading = true)]
    [Guid(LsVisualStudioPackage.PackageGuidString)]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
    // This autoload doesn't happen when opening a solution file directly.
    [ProvideAutoLoad(VSConstants.UICONTEXT.SolutionOpening_string, PackageAutoLoadFlags.BackgroundLoad)]
    // This autoload occurs after Visual Studio has completely started. It would be nice
    // if the autoload would occur earlier.
    [ProvideAutoLoad(VSConstants.UICONTEXT.ShellInitialized_string, PackageAutoLoadFlags.BackgroundLoad)]
    public sealed class LsVisualStudioPackage : AsyncPackage
    {
        /// <summary>
        /// LsVisualStudioPackage GUID string.
        /// </summary>
        public const string PackageGuidString = "4c68a5e8-056b-4fe9-b381-50208f5e8f35";

        private bool _initialized = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="LsVisualStudioPackage"/> class.
        /// </summary>
        public LsVisualStudioPackage()
        {
            // Inside this method you can place any initialization code that does not require
            // any Visual Studio service because at this point the package object is created but
            // not sited yet inside Visual Studio environment. The place to do all the other
            // initialization is the Initialize method.
        }

        protected override async Task InitializeAsync(CancellationToken cancellationToken, IProgress<ServiceProgressData> progress)
        {
            // Run this code in the background to avoid VS notifing the user a background
            // task is still running. Also makes VS start faster than if it was inline.
            var task = new Func<Task>(async () => {
                if (_initialized)
                    return;

                // Since this package might not be initialized until after a solution has finished loading,
                // we need to check if a solution has already been loaded and then handle it.
                bool isSolutionLoaded = await IsSolutionLoadedAsync();

                if (isSolutionLoaded)
                {
                    HandleOpenSolution();
                }

                // Listen for subsequent solution events
                SolutionEvents.OnAfterOpenSolution += HandleOpenSolution;
                SolutionEvents.OnBeforeCloseSolution += HandleBeforeCloseSolution;

                _initialized = true;
            });

            await task.Invoke();
        }

        private string GetSolutionName(string solutionFullName)
        {
            var solutionName = solutionFullName;

            if (solutionName.ToLower().EndsWith(".sln"))
                solutionName = Path.GetFileNameWithoutExtension(solutionName);

            return solutionName;
        }

        private void HandleBeforeCloseSolution(object sender, EventArgs e)
        {
            if (!Settings.Instance.Configured)
                return;

            var l = new Func<Task>(async () =>
            {
                await JoinableTaskFactory.SwitchToMainThreadAsync();

                var dte2 = (DTE2)Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(SDTE));
                if (dte2 == null)
                    return;

                var solutionName = GetSolutionName(dte2.Solution.FullName);

                await LsClientManager.Instance.DisposeClientAsync(solutionName);
            });

            _ = l.Invoke();
        }

        private async Task<bool> IsSolutionLoadedAsync()
        {
            await JoinableTaskFactory.SwitchToMainThreadAsync();
            var solService = await GetServiceAsync(typeof(SVsSolution)) as IVsSolution;

            if (solService == null)
                return false;

            ErrorHandler.ThrowOnFailure(solService.GetProperty((int)__VSPROPID.VSPROPID_IsSolutionOpen, out object value));

            return value is bool isSolOpen && isSolOpen;
        }

        private void HandleOpenSolution(object sender = null, EventArgs e = null)
        {
            // Handle the open solution and try to do as much work
            // on a background thread as possible

            if (!Settings.Instance.Configured)
                return;

            var l = new Func<Task>(async () =>
            {
                await JoinableTaskFactory.SwitchToMainThreadAsync();

                var dte2 = (DTE2)Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(SDTE));
                if (dte2 == null)
                    return;

                var solutionName = GetSolutionName(dte2.Solution.FullName);
                var solutionPath = Path.GetDirectoryName(dte2.Solution.FileName);

                LsClientManager.Instance.GetClient(solutionName, solutionPath);
            });

            _ = l.Invoke();
        }
    }
}
