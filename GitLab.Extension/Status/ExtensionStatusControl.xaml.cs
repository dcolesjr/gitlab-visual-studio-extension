﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GitLab.Extension.Status
{
    /// <summary>
    /// Interaction logic for CodeSuggestionsStatusControl.xaml
    /// </summary>
    public partial class ExtensionStatusControl : UserControl
    {
        public delegate void MenuItemClickHandler(ExtensionStatusControl control);

        private MenuItemClickHandler _menuItemClickHandler;
        private readonly BitmapImage _imageEnabled;
        private readonly BitmapImage _imageDisabled;
        private readonly BitmapImage _imageError;
        private readonly BitmapImage _imageLoading;
        private const string EnabledTooltip = "GitLab code suggestions are enabled. Click to disable.";
        private const string DisabledTooltip = "GitLab code suggestions are disabled. Click to enable.";
        private const string ErrorTooltip = "Error: {0}";

        public ExtensionStatusControl(MenuItemClickHandler menuItemClickHandler)
        {
            InitializeComponent();
            _menuItemClickHandler = menuItemClickHandler;

            try
            {
                _imageEnabled = LoadImageFromResource("Enabled.png");
                _imageDisabled = LoadImageFromResource("Disabled.png");
                _imageError = LoadImageFromResource("Error.png");
                _imageLoading = LoadImageFromResource("Loading.png");

                ImageStatus.Source = _imageEnabled;
                ButtonStatus.ToolTip = EnabledTooltip;
            }
            catch (Exception ex)
            {
                Trace.WriteLine($"Status: Exception {ex}");
            }
        }

        private static BitmapImage LoadImageFromResource(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();

            var resourceName = assembly.GetManifestResourceNames().SingleOrDefault(name => name.EndsWith(fileName));
            if (resourceName == null)
                return null;

            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = stream;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();

                return bitmap;
            }
        }

        private void ButtonStatus_Click(object sender, RoutedEventArgs e)
        {
            if (_menuItemClickHandler != null)
                _menuItemClickHandler(this);
        }

        public void StatusEnabled()
        {
            ImageStatus.Source = _imageEnabled;
            ButtonStatus.ToolTip = EnabledTooltip;
        }

        public void StatusDisabled()
        {
            ImageStatus.Source = _imageDisabled;
            ButtonStatus.ToolTip = DisabledTooltip;
        }

        public void StatusError(string message)
        {
            if (message == null)
                message = string.Empty;

            ImageStatus.Source = _imageError;
            ButtonStatus.ToolTip = string.Format(
                $"{EnabledTooltip}\n{ErrorTooltip}", message);
        }

        public void StatusLoading()
        {
            ImageStatus.Source = _imageLoading;
        }
    }
}
