﻿using Microsoft.VisualStudio.PlatformUI;
using System;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using GitLab.Extension.SettingsUtil;

namespace GitLab.Extension.Status
{
    /// <summary>
    /// Display a GitLab status icon in the status bar
    /// </summary>
    internal class StatusBar
    {
        /// <summary>
        /// Singleton instance
        /// </summary>
        public static readonly StatusBar Instance = new StatusBar();

        /// <summary>
        /// How long to wait before clearing temporary states Error and
        /// InProgress.
        /// </summary>
        private const int ClearTemporaryStateWaitTime = 1000 * 30;

        private readonly object _clearTemporaryStateTimerLock = new object();
        private readonly object _initializeLock = new object();

        private bool _initialized = false;
        private bool _statusBarEnabled = true;
        private ExtensionStatusControl _codeSuggestionsStatusControl;
        private Timer _clearTemporaryStateTimer;

        public bool CodeSuggestionsEnabled { get; private set; }

        private StatusBar()
        {
        }

        /// <summary>
        /// Initialize the display of our status bar icon
        /// </summary>
        /// <param name="uiElement"></param>
        public void InitializeDisplay(UIElement uiElement)
        {
            lock (_initializeLock)
            {
                if (_initialized)
                    return;
                _initialized = true;
            }

            try
            {
#pragma warning disable VSTHRD001 // Avoid legacy thread switching APIs
                uiElement.Dispatcher.Invoke(new Action(() =>
                {
                    var app = Application.Current;
                    var statusBarPanel = app.MainWindow.FindDescendant<DockPanel>(
                        new Predicate<DockPanel>((p) => { return p.Name == "StatusBarPanel"; }));

                    _codeSuggestionsStatusControl = new ExtensionStatusControl(StatusIconClickHandler);

                    DockPanel.SetDock(_codeSuggestionsStatusControl, Dock.Right);
                    statusBarPanel.Children.Add(_codeSuggestionsStatusControl);
                }));
#pragma warning restore VSTHRD001 // Avoid legacy thread switching APIs
            }
            catch
            {
                // TODO - Log Error
            }
        }

        /// <summary>
        /// Called when user clicks on the code suggestions
        /// status icon in the status bar.
        /// </summary>
        /// <param name="control"></param>
        private void StatusIconClickHandler(ExtensionStatusControl control)
        {
            if (_statusBarEnabled)
            {
                _statusBarEnabled = false;
                control.StatusDisabled();
                Settings.Instance.IsCodeSuggestionsEnabled = false;

                return;
            }

            _statusBarEnabled = true;
            control.StatusEnabled();
            Settings.Instance.IsCodeSuggestionsEnabled = true;
        }

        /// <summary>
        /// Set error icon and update tooltip message
        /// </summary>
        /// <param name="message">Error message to display</param>
        /// <param name="persistantError">Display error until told not too.</param>
        public void CodeSuggestionsError(string message, bool persistantError = false)
        {
            if (!_initialized)
                return;

#pragma warning disable VSTHRD001 // Avoid legacy thread switching APIs
            _codeSuggestionsStatusControl.Dispatcher.Invoke(new Action(() =>
            {
                _codeSuggestionsStatusControl.StatusError(message);
            }));
#pragma warning restore VSTHRD001 // Avoid legacy thread switching APIs

            if(!persistantError)
                StartClearTemporaryStateTimer();
        }

        /// <summary>
        /// Stop showing error
        /// </summary>
        public void CodeSuggestionsClearError()
        {
            if (!_initialized)
                return;

            StopClearTemporaryStateTimer();

#pragma warning disable VSTHRD001 // Avoid legacy thread switching APIs
            _codeSuggestionsStatusControl.Dispatcher.Invoke(new Action(() =>
            {
                if (_statusBarEnabled)
                {
                    _codeSuggestionsStatusControl.StatusEnabled();
                }
                else
                {
                    _codeSuggestionsStatusControl.StatusDisabled();
                }
            }));
#pragma warning restore VSTHRD001 // Avoid legacy thread switching APIs
        }

        /// <summary>
        /// Start showing suggestion in progress icon
        /// </summary>
        public void CodeSuggestionsInProgressStart()
        {
            if (!_initialized)
                return;

#pragma warning disable VSTHRD001 // Avoid legacy thread switching APIs
            _codeSuggestionsStatusControl.Dispatcher.Invoke(new Action(() =>
            {
                _codeSuggestionsStatusControl.StatusLoading();
            }));
#pragma warning restore VSTHRD001 // Avoid legacy thread switching APIs

            StartClearTemporaryStateTimer();
        }

        /// <summary>
        /// Stop showing suggestion in progress icon
        /// </summary>
        public void CodeSuggestionsInProgressComplete()
        {
            if (!_initialized)
                return;

            StopClearTemporaryStateTimer();

#pragma warning disable VSTHRD001 // Avoid legacy thread switching APIs
            _codeSuggestionsStatusControl.Dispatcher.Invoke(new Action(() =>
            {
                if(_statusBarEnabled)
                {
                    _codeSuggestionsStatusControl.StatusEnabled();
                }
                else
                {
                    _codeSuggestionsStatusControl.StatusDisabled();
                }
            }));
#pragma warning restore VSTHRD001 // Avoid legacy thread switching APIs
        }

        /// <summary>
        /// Start a timer to clear display of a temporary state
        /// </summary>
        private void StartClearTemporaryStateTimer()
        {
            lock (_clearTemporaryStateTimerLock)
            {
                if (_clearTemporaryStateTimer != null)
                {
                    _clearTemporaryStateTimer.Stop();
                    _clearTemporaryStateTimer.Dispose();
                }

                _clearTemporaryStateTimer = new Timer(ClearTemporaryStateWaitTime);
                _clearTemporaryStateTimer.Elapsed += clearTemporaryStateTimer_Elapsed;
                _clearTemporaryStateTimer.AutoReset = false;
                _clearTemporaryStateTimer.Enabled = true;
            }
        }

        /// <summary>
        /// Stop timer to clear display of a temporary state
        /// </summary>
        private void StopClearTemporaryStateTimer()
        {
            lock (_clearTemporaryStateTimerLock)
            {
                if (_clearTemporaryStateTimer == null)
                    return;

                _clearTemporaryStateTimer.Stop();
                _clearTemporaryStateTimer.Dispose();
                _clearTemporaryStateTimer = null;
            }
        }

        /// <summary>
        /// Called when timer elapses
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clearTemporaryStateTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
#pragma warning disable VSTHRD001 // Avoid legacy thread switching APIs
            _codeSuggestionsStatusControl.Dispatcher.Invoke(new Action(() =>
            {
                if (_statusBarEnabled)
                {
                    _codeSuggestionsStatusControl.StatusEnabled();
                }
                else
                {
                    _codeSuggestionsStatusControl.StatusDisabled();
                }
            }));
#pragma warning restore VSTHRD001 // Avoid legacy thread switching APIs
        }
    }
}
